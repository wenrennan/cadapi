## 前言

会持续更新CAD二次开发api功能介绍和视频，有问题可以私信联系我，也可以加群交流

[闻人南131的个人空间_哔哩哔哩_bilibili](https://space.bilibili.com/2114059610)

![输入图片说明](png/%E7%BE%A4%E4%BA%8C%E7%BB%B4%E7%A0%81.png)

# 曲线Curve

| 属性       | 数据类型 | 说明                                                         |
| :--------- | -------- | ------------------------------------------------------------ |
| Area       | double   | 面积<br />直线：0<br />圆：面积<br />圆弧：起点终点连起来闭合区域<br />椭圆：面积<br />椭圆弧：起点终点连起来闭合区域<br />多段线：闭合的话就是闭合区域，<br />不闭合的话就是起点终点连起来闭合区域 |
| Spline     | Spline   | 拟合样条曲线（不懂）                                         |
| EndPoint   | Point3d  | 终点                                                         |
| StartPoint | Point3d  | 起点                                                         |
| EndParam   | double   | 终点参数                                                     |
| StartParam | double   | 起点参数<br />直线：0-长度<br />圆：0-2π<br />圆弧：<br />椭圆：0-2π<br />椭圆弧：<br />多段线：0-分段数，所以可以用点参数来<br />判断在多段线的第几段上 |
| IsPeriodic | bool     | 周期性（不懂）                                               |
| Closed     | bool     | 是否闭合，（圆和椭圆为闭合<br />多段线看具体情况，<br />直线，圆弧，椭圆弧不闭合） |

| 方法                                                         | 参数                                                         | 说明                                                         |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Point3d [GetPointAtParameter](#Curve1)<br/>(<br/>&ensp;   double value<br/>) | &ensp;<br />&ensp;<br />参数<br /> &ensp;                    | 获取指定参数处的点                                           |
| double [GetParameterAtPoint](#Curve2)<br/>(<br/>&ensp;   Point3d point<br/>) | &ensp;<br />&ensp;<br />点<br /> &ensp;                      | 获取指定点处的参数                                           |
| double [GetDistanceAtParameter](#Curve3)<br/>(<br/>&ensp;   double value<br/>) | &ensp;<br />&ensp;<br />参数<br /> &ensp;                    | 获取指定参数处的距离                                         |
| double [GetParameterAtDistance](#Curve4)<br/>(<br/>&ensp;   double dist<br/>) | &ensp;<br />&ensp;<br />距离<br /> &ensp;                    | 获取指定距离处的参数                                         |
| double [GetDistAtPoint](#Curve4)<br/>(<br/>&ensp;   Point3d point<br/>) | &ensp;<br />&ensp;<br />点<br /> &ensp;                      | 获取指定点出的距离                                           |
| Point3d [GetPointAtDist](#Curve6)<br/>(<br/>&ensp;   double value<br/>) | &ensp;<br />&ensp;<br />距离<br /> &ensp;                    | 获取指定距离处的点                                           |
| Vector3d [GetFirstDerivative](#Curve7)<br/>(<br/>&ensp;   Point3d point<br/>) | &ensp;<br />&ensp;<br />点<br /> &ensp;                      | 获取指定点处的一阶导数的向量                                 |
| Vector3d [GetFirstDerivative](#Curve8)<br/>(<br/>&ensp;   double value<br/>) | &ensp;<br />&ensp;<br />起点<br /> &ensp;                    | 获取指定参数处的一阶导数的向量                               |
| Vector3d [GetSecondDerivative](#Curve9)<br/>(<br/>&ensp;   Point3d point<br/>) | &ensp;<br />&ensp;<br />起点<br /> &ensp;                    | 获取指定点处的二阶导数的向量                                 |
| Vector3d [GetSecondDerivative](#Curve10)<br/>(<br/>&ensp;   double value<br/>) | &ensp;<br />&ensp;<br />起点<br /> &ensp;                    | 获取指定参数处的二阶导数的向量                               |
| Point3d [GetClosestPointTo](#Curve11)<br/>(<br/>&ensp;   Point3d givenPoint,<br/>&ensp;   Vector3d direction,<br/>&ensp;   bool extend<br/>) | &ensp;<br />&ensp;<br />点<br />向量<br />是否延长<br /> &ensp; | 获取指定点在指定向量方向上，到这条线<br />的最近点，可以选择是否延长这条线 |
| Point3d [GetClosestPointTo](#Curve12)<br/>(<br/>&ensp;   Point3d givenPoint,<br/>&ensp;   bool extend<br/>) | &ensp;<br />&ensp;<br />点<br />是否延长<br /> &ensp;        | 获取指定点，到这条线的最近点<br />，可以选择是否延长这条线   |
| Curve [GetOrthoProjectedCurve](#Curve13)<br/>(<br/>&ensp;   Plane planeToProjectOn<br/>) | &ensp;<br />&ensp;<br />面<br /> &ensp;                      | 获取在指定面上的投影曲线                                     |
| Curve [GetProjectedCurve](#Curve14)<br/>(<br/>&ensp;   Plane planeToProjectOn,<br/>&ensp;   Vector3d projectionDirection<br/>) | &ensp;<br />&ensp;<br />面<br />向量<br /> &ensp;            | 获取在指定面上，指定向量的投影曲线                           |
| DBObjectCollection [GetOffsetCurves](#Curve15)<br/>(<br/>&ensp;   double offsetDist<br/>) | &ensp;<br />&ensp;<br />偏移距离<br /> &ensp;                | 获得偏移指定距离的曲线                                       |
| DBObjectCollection <br />[GetOffsetCurvesGivenPlaneNormal](#Curve16)<br/>(<br/>&ensp;   Vector3d normal,<br/>&ensp;   double offsetDist<br/>) | &ensp;<br />&ensp;<br /><br />向量<br />偏移距离<br /> &ensp; | 指定向量为法向向量的平面上，<br />进行指定距离的偏移         |
| DBObjectCollection [GetSplitCurves](#Curve17)<br/>(<br/>&ensp;   Point3dCollection points<br/>) | &ensp;<br />&ensp;<br />点集<br /> &ensp;                    | 根据点集打断曲线<br />注意1：闭合曲线起码两个点<br />注意2：多个点的是注意根据到起点距离排序 |
| DBObjectCollection [GetSplitCurves](#Curve18)<br/>(<br/>&ensp;   DoubleCollection value<br/>) | &ensp;<br />&ensp;<br />参数集<br /> &ensp;                  | 根据参数分割曲线，注意事项同上                               |
| void [Extend](#Curve19)<br/>(<br/>&ensp;   bool extendStart,<br/>&ensp;   Point3d toPoint<br/>) | &ensp;<br />&ensp;<br />是否延长起点<br />目标点<br /> &ensp; | 注意点要在延长线上                                           |
| void [Extend](#Curve20)<br/>(<br/>&ensp;   double newParameter<br/>) | &ensp;<br />&ensp;<br />参数<br /> &ensp;                    | 延长到指定参数处                                             |
| void [ReverseCurve](#Curve21)(  )                            |                                                              | 曲线转向                                                     |
| Curve3d [GetGeCurve](#Curve22)( )                            |                                                              | 获取ge曲线                                                   |
| Curve3d [GetGeCurve](#Curve23)<br/>(<br/>&ensp;   Tolerance tolerance<br/>) | &ensp;<br />&ensp;<br />容差<br /> &ensp;                    | 不懂                                                         |
| Curve [CreateFromGeCurve](#Curve24)<br/>(<br/>&ensp;   Curve3d geCurve<br/>) | &ensp;<br />&ensp;<br />ge曲线<br /> &ensp;                  | ge曲线转db曲线                                               |
| Curve [CreateFromGeCurve](#Curve25)<br/>(<br/>&ensp;   Curve3d geCurve,<br/>&ensp;   Tolerance tolerance<br/>) | &ensp;<br />&ensp;<br />ge曲线<br />容差<br /> &ensp;        | 不懂                                                         |
| Curve [CreateFromGeCurve](#Curve26)<br/>(<br/>&ensp;   Curve3d geCurve,<br/>&ensp;   Vector3d _unnamed001<br/>) | &ensp;<br />&ensp;<br />ge曲线<br />向量<br /> &ensp;        | 不懂                                                         |
| Curve [CreateFromGeCurve](#Curve27)<br/>(<br/>&ensp;   Curve3d geCurve,<br/>&ensp;   Vector3d __unnamed001,<br/>&ensp;   Tolerance tolerance<br/>) | &ensp;<br />&ensp;<br />ge曲线<br />向量<br />容差<br /> &ensp; | 不懂                                                         |
| void [SetFromGeCurve](#Curve28)<br/>(<br/>&ensp;   Curve3d geCurve<br/>) | &ensp;<br />&ensp;<br />ge曲线<br /> &ensp;                  | 根据ge曲线设置db曲线                                         |
| void SetFromGeCurve<br/>(<br/>&ensp;   Curve3d geCurve,<br/>&ensp;   Tolerance tolerance<br/>) | &ensp;<br />&ensp;<br />ge曲线<br />容差<br /> &ensp;        | 不懂                                                         |
| void SetFromGeCurve<br/>(<br/>&ensp;   Curve3d geCurve,<br/>&ensp;   Vector3d __unnamed001<br/>) | &ensp;<br />&ensp;<br />ge曲线<br />向量<br /> &ensp;        | 不懂                                                         |
| void SetFromGeCurve<br/>(<br/>&ensp;   Curve3d geCurve,<br/>&ensp;   Vector3d __unnamed001,<br/>&ensp;   Tolerance tolerance<br/>) | &ensp;<br />&ensp;<br />ge曲线<br />向量<br />容差<br /> &ensp; | 不懂                                                         |

<a name="Curve1"></a>

```c#
Point3d point = curve.GetPointAtParameter(5);
Circle circle = new Circle(point, Vector3d.ZAxis, 0.2);
circle.ColorIndex = 2;
circle.LineWeight = LineWeight.LineWeight015;
circle.AddEntity();
```

<a name="Curve2"></a>

```c#
Point3d point = new Point3d(5, 5, 0);
double par = curve.GetParameterAtPoint(point);
```

<a name="Curve3"></a>

```c#
double dis = curve.GetDistanceAtParameter(2.5);
```

<a name="Curve4"></a>

```c#
double par = curve.GetParameterAtDistance(7);
```

<a name="Curve5"></a>

```c#
Point3d point = new Point3d(3,4,0);
double dis = curve.GetDistAtPoint(point);
```

<a name="Curve6"></a>

```c#
Point3d point = curve.GetPointAtDist(9.5);
```

<a name="Curve7"></a>

```c#
Vector3d vector = curve.GetFirstDerivative(new Point3d(3,0,0));
```

<a name="Curve8"></a>

```c#
Vector3d vector = curve.GetFirstDerivative(1.5);
```

<a name="Curve9"></a>

```c#
Vector3d vector = curve.GetSecondDerivative(new Point3d(3, 2, 0));
```

<a name="Curve10"></a>

```c#
Vector3d vector = curve.GetSecondDerivative(Math.PI);
```

<a name="Curve11"></a>

```c#
Point3d point = new Point3d(1.5, 1, 0);
Point3d p = curve.GetClosestPointTo(point, new Vector3d(1, 0, 0), false);
```

<a name="Curve12"></a>

```c#
Point3d point = new Point3d(1.5, 1, 0);
Point3d p = curve.GetClosestPointTo(point, false);
```

<a name="Curve13"></a>

```c#
Plane plane = new Plane(new Point3d(),new Vector3d(1,0,1));
curve = curve.GetOrthoProjectedCurve(plane);
```

<a name="Curve14"></a>

```c#
Plane plane = new Plane(new Point3d(0,0,1),Vector3d.ZAxis);
curve = curve.GetProjectedCurve(plane,new Vector3d(0,0,2));
```

<a name="Curve15"></a>

```c#
DBObjectCollection dbs = curve.GetOffsetCurves(1);
```

<a name="Curve16"></a>

```c#
DBObjectCollection dbs = curve.GetOffsetCurvesGivenPlaneNormal(new Vector3d(0, 0, -1),1);
```

<a name="Curve17"></a>

```c#
Point3dCollection pos = new Point3dCollection();
double dis = curve.GetDistanceAtParameter(curve.EndParam);
pos.Add(curve.GetPointAtDist(dis / 3));
pos.Add(curve.GetPointAtDist(dis / 3 * 2));
DBObjectCollection dbs = curve.GetSplitCurves(pos);
```

<a name="Curve18"></a>

```c#
DoubleCollection ds = new DoubleCollection();
double par = curve.EndParam - curve.StartParam;
ds.Add(curve.StartParam + par / 3);
ds.Add(curve.StartParam + par / 3 * 2);
DBObjectCollection dbs = curve.GetSplitCurves(ds);
```

<a name="Curve19"></a>

```c#
curve.Extend(false, new Point3d(15, 15, 0));
```

<a name="Curve20"></a>

```c#
curve.Extend(2);
```

<a name="Curve21"></a>

```c#
curve.ReverseCurve();
```

<a name="Curve22"></a>

```c#
Curve3d curve3d=curve.GetGeCurve();
```

<a name="Curve23"></a>

```c#
不懂
```

<a name="Curve24"></a>

```c#
LineSegment3d line3d = new LineSegment3d(new Point3d(), new Point3d(10, 10, 0));
Line line = Curve.CreateFromGeCurve(line3d) as Line;
```

<a name="Curve25"></a>

```c#
不懂
```

<a name="Curve26"></a>

```c#
不懂
```

<a name="Curve27"></a>

```c#
不懂
```

<a name="Curve28"></a>

```c#
LineSegment3d line3d = new LineSegment3d(new Point3d(), new Point3d(10, 10, 0));
Line line = new Line(new Point3d(5, 5, 0), new Point3d(20, 0, 0));
line.SetFromGeCurve(line3d);
```

<a name="Curve29"></a>

```c#
不懂
```

<a name="Curve30"></a>

```c#
不懂
```

<a name="Curve31"></a>

```c#
不懂
```



------

## 直线 Line 

https://www.bilibili.com/video/BV1Ls4y1o7kh/

![](png/Line.png)

| 属性       | 中文     | 数据类型 | 作用                       |
| :--------- | -------- | -------- | -------------------------- |
| Length     | 长度     | double   | 直线的长度                 |
| Angle      | 角度     | double   | 直线的弧度，0~2π           |
| Delta      | 增量     | Vector3d | 起点到终点的向量           |
| Normal     | 法向向量 | Vector3d | 直线所在平面的法向单位向量 |
| Thickness  | 厚度     | double   |                            |
| EndPoint   | 终点     | Point3d  | 直线的终点                 |
| StartPoint | 起点     | Point3d  | 直线的起点                 |

| 方法                                                         | 参数                                                | 说明                                 |
| ------------------------------------------------------------ | --------------------------------------------------- | ------------------------------------ |
| [Line( )](#Line1)                                            | 无参数                                              | 构造函数：声明一条空的直线           |
| [Line](#Line2)<br />(<br />&ensp;   Point3d pointer1, <br />&ensp;   Point3d pointer2<br />) | &ensp;<br />&ensp;<br />起点<br />终点<br /> &ensp; | 构造函数：声明从起点到终点的一条直线 |

<a name="Line1"></a>

```c#
Line line = new Line();
```

<a name="Line2"></a>

```c#
Point3d point1 = new Point3d(0, 0, 0);
Point3d point2 = new Point3d(10, 10, 0);
Line line = new Line(point1, point2);
```

------

## 圆 Circle

https://www.bilibili.com/video/BV1gY411673D/

![](png/Circle.png)

| 属性          | 中文         | 数据类型 | 作用                       |
| :------------ | ------------ | -------- | -------------------------- |
| Diameter      | 直径         | double   | 圆的直径                   |
| Circumference | 周长         | double   | 圆的周长                   |
| Normal        | 单位法向向量 | Vector3d | 圆所在的平面的单位法向向量 |
| Thickness     | 厚度         | double   | 圆的厚度                   |
| Radius        | 半径         | double   | 圆的半径                   |
| Center        | 圆心         | Point3d  | 圆的圆心                   |

| 方法                                                         | 参数                                                         | 说明                               |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ---------------------------------- |
| [Circle( )](#Circle1)                                        | 无参数                                                       | 构造函数：声明一个空的圆           |
| [Circle](#Circle2)<br />(<br />&ensp;   Point3d center, <br />&ensp;   Vector3d normal,<br />&ensp;   double radius<br />) | &ensp;<br />&ensp;<br />圆心<br />法向向量<br />半径<br /> &ensp; | 构造函数：声明一个确定圆心半径的圆 |

<a name="Circle1"></a>

```c#
Circle circle = new Circle();
```

<a name="Circle2"></a>

```c#
Point3d center = new Point3d(0, 0, 0);
Vector3d normal = Vector3d.ZAxis;
double radius = 5;
Circle circle = new Circle(center, normal, radius);
```

------

## 圆弧 Arc

https://www.bilibili.com/video/BV1fX4y1S7ad/

![](png/Arc.png)

| 属性       | 中文     | 数据类型 | 作用                       |
| :--------- | -------- | -------- | -------------------------- |
| TotalAngle | 总角度   | double   | 圆弧的总弧度               |
| Length     | 总长     | double   | 圆弧的总长度               |
| Normal     | 法向向量 | Vector3d | 圆弧所在平面的单位法向向量 |
| Thickness  | 厚度     | double   | 圆弧的厚度                 |
| EndAngle   | 终点角度 | double   | 圆心到终点连线的弧度       |
| StartAngle | 起点角度 | double   | 圆心到起点连线的弧度       |
| Radius     | 半径     | double   | 圆弧的半径                 |
| Center     | 圆心     | Point3d  | 圆弧的圆心                 |

**注意事项**：当Normal为-Z轴方向时，虽然圆弧任是根据右手定则逆时针的，但是从用户视角，圆弧变成了顺时针，圆弧起始弧度判断也从X轴变成了-X轴，在读取一些图纸中的圆弧时，需注意Normal方向。

| 方法                                                         | 参数                                                         | 说明                                                         |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| [Arc( )](#Arc1)                                              | 无参数                                                       | 构造函数：声明一个空的圆弧                                   |
| [Arc](#Arc2)<br />(<br />&ensp;   Point3d center, <br />&ensp;   double radius,<br />&ensp;   double startAngle,<br />&ensp;   double endAngle<br />) | &ensp;<br />&ensp;<br />圆心<br />半径<br />起点角度<br />终点角度<br /> &ensp; | 构造函数：声明一个确定<br />圆心半径的起点终点的圆弧         |
| [Arc](#Arc3)<br />(<br />&ensp;   Point3d center, <br />&ensp;   Vector3d normal,<br />&ensp;   double radius,<br />&ensp;   double startAngle,<br />&ensp;   double endAngle<br />) | &ensp;<br />&ensp;<br />圆心<br />法向向量<br />半径<br />起点角度<br />终点角度<br /> &ensp; | 构造函数：声明一个确定<br />圆心半径的起点终点的圆弧，<br />并且设置法向向量，可以控制圆弧的顺逆时针 |

<a name="Arc1"></a>

```c#
Arc arc = new Arc();
```

<a name="Arc2"></a>

```C#
Point3d center = Point3d.Origin;
double radius = 5;
double startAngle = 0;
double endAngle = Math.PI / 3;
Arc arc = new Arc(center, radius, startAngle, endAngle);
```

<a name="Arc3"></a>

```c#
Point3d center = Point3d.Origin;
Vector3d normal = -Vector3d.ZAxis;
double radius = 5;
double startAngle = 0;
double endAngle = Math.PI / 3;
Arc arc = new Arc(center, normal, radius, startAngle, endAngle);
```

------

## 椭圆 Ellipse

https://www.bilibili.com/video/BV1HL41117dS/

![椭圆](png/Ellipse.png)

| 属性        | 中文     | 数据类型 | 作用                       |
| :---------- | -------- | -------- | -------------------------- |
| MinorRadius | 短轴半径 | double   | 椭圆短轴的半径             |
| MajorRadius | 长轴半径 | double   | 椭圆长轴的半径             |
| IsNull      | 是否为空 | bool     | 判断椭圆是否为空           |
| EndParam    | 终点参数 | double   | 椭圆终点的参数             |
| StartParam  | 起点参数 | double   | 椭圆起点的参数             |
| EndAngle    | 终点角度 | double   | 椭圆终点的弧度             |
| StartAngle  | 起点角度 | double   | 椭圆起点的弧度             |
| RadiusRatio | 半径比例 | double   | 短轴半径/长轴半径          |
| MinorAxis   | 短轴向量 | Vector3d | 椭圆短轴的向量             |
| MajorAxis   | 长轴向量 | Vector3d | 椭圆长轴的方向             |
| Normal      | 法向向量 | Vector3d | 椭圆所在平面的单位法向向量 |
| Center      | 圆心     | Point3d  | 椭圆的圆心                 |

| 方法                                                         | 参数                                                         | 说明                             |
| ------------------------------------------------------------ | ------------------------------------------------------------ | -------------------------------- |
| [Ellipse( )](#Ellipse1)                                      | 无参数                                                       | 构造函数：声明一个空的椭圆       |
| [Ellipse](#Ellipse2)<br />(<br />&ensp;   Point3d center, <br />&ensp;   Vector3d unitNormal,<br />&ensp;   Vector3d majorAxis,<br />&ensp;   double radiusRatio,<br />&ensp;   double startAngle,<br />&ensp;   double endAngle<br />) | &ensp;<br />&ensp;<br />圆心<br />法向向量<br />长轴向量<br />半径比例<br />起始弧度<br />终止弧度<br /> &ensp; | 构造函数：声明一个确定参数的椭圆 |
| void [Set](#Ellipse3)<br />(<br />&ensp;   Point3d center, <br />&ensp;   Vector3d unitNormal,<br />&ensp;   Vector3d majorAxis,<br />&ensp;   double radiusRatio,<br />&ensp;   double startAngle,<br />&ensp;   double endAngle<br />) | &ensp;<br />&ensp;<br />圆心<br />法向向量<br />长轴向量<br />半径比例<br />起始弧度<br />终止弧度<br /> &ensp; | 给一个椭圆设置参数               |
| double [GetParameterAtAngle](#Ellipse4)<br />(<br />    double angle<br />) | &ensp;<br />&ensp;<br />角度<br />                           | 获取指定角度处的参数             |
| double [GetAngleAtParameter](#Ellipse5)<br />(<br />    double value<br />) | &ensp;<br />&ensp;<br />参数<br />                           | 获取指定参数处的角度             |

<a name="Ellipse1"></a>

```c#
Ellipse ellipse = new Ellipse();
```

<a name="Ellipse2"></a>

```c#
Point3d center = new Point3d(0, 0, 0);
Vector3d unitNormal= Vector3d.ZAxis;
Vector3d majorAxis = new Vector3d(10, 0, 0);
double radiusRatio = 0.5;
double startAngle = 0;
double endAngle = Math.PI * 2;
Ellipse ellipse = new Ellipse(center, unitNormal, majorAxis, radiusRatio, startAngle, endAngle);
```

<a name="Ellipse3"></a>

```c#
Ellipse ellipse = new Ellipse();
Point3d center = new Point3d(0, 0, 0);
Vector3d unitNormal = Vector3d.ZAxis;
Vector3d majorAxis = new Vector3d(10, 0, 0);
double radiusRatio = 0.5;
double startAngle = Math.PI / 3;
double endAngle = Math.PI / 3 * 2;
ellipse.Set(center, unitNormal, majorAxis, radiusRatio, startAngle, endAngle);
```

<a name="Ellipse4"></a>

```C#
double value = ellipse.GetParameterAtAngle(Math.PI / 3);
```

<a name="Ellipse5"></a>

```C#
double angle = ellipse.GetAngleAtParameter(2);
```

------

## 多段线 Polyline

https://www.bilibili.com/video/BV11g4y1E7iH/

| 属性             | 中文         | 数据类型 | 作用                                             |
| :--------------- | ------------ | -------- | ------------------------------------------------ |
| Length           | 长度         | double   | 多段线的长度                                     |
| HasWidth         | 是否有宽度   | bool     | 多段线是否有宽度                                 |
| HasBulges        | 是否有凸度   | bool     | 多段线是否有圆弧                                 |
| NumberOfVertices | 节点数       | int      | 多段线的节点数量                                 |
| IsOnlyLines      | 是否只有直线 | bool     | 多段线是否全部由直线组成                         |
| Normal           | 法向向量     | Vector3d | 多段线所在平面的单位法向向量                     |
| ConstantWidth    | 全局宽度     | double   | 多段线的全局宽度，宽度一致                       |
| Thickness        | 厚度         | double   | 多段线的厚度                                     |
| Elevation        | 高程         | double   | 多段线的高程，即Z坐标                            |
| Plinegen         | 普林根       | bool     | 关闭时节点会打断线型<br />打开时节点不会打断线型 |
| Closed           | 是否闭合     | bool     | 多段线是否闭合                                   |

| 方法                                                         | 参数                                                         | 说明                                                         |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| [Polyline( )](#Polyline1)                                    | 无参数                                                       | 构造函数：声明一个空的多段线                                 |
| [Polyline](#Polyline2)<br />(<br />&ensp;   int vertices<br />) | &ensp;   <br />&ensp;   <br />节点数<br />                   | 构造函数：声明一个确定节点<br />数的多段线                   |
| void [ConvertFrom](#Polyline3)<br />(<br />&ensp;   Entity entity,<br />&ensp;   bool transferId<br />) | &ensp;   <br />&ensp;   <br />转化源<br />是否传递ID<br />&ensp; | 从别的Entity转成Polyline<br />（目前只发现可以从<br />Polyline2d转） |
| Polyline2d [ConvertTo](#Polyline4)<br />(<br />&ensp;   bool transferId<br />) | &ensp;   <br />&ensp;   <br />是否传递ID<br />&ensp;         | 把Polyline转成Polyline2d<br />参数涉及IdMapping<br />（目前我还不懂嘿嘿嘿） |
| Point3d [GetPoint3dAt](#Polyline5)<br />(<br />&ensp;   int value<br />) | &ensp;   <br />&ensp;   <br />节点索引<br />&ensp;           | 获取指定节点索引处的点<br />注意索引不要超出                 |
| SegmentType [GetSegmentType](#Polyline6)<br />(<br />&ensp;   int index<br />) | &ensp;   <br />&ensp;   <br />段落索引<br />&ensp;           | 获取指定索引处的段落<br />的类型(线、圆弧等等)               |
| LineSegment2d [GetLineSegment2dAt](#Polyline7)<br />(<br />&ensp;   int index<br />) | &ensp;   <br />&ensp;   <br />段落索引<br />&ensp;           | 获取指定索引处的段落<br />的二维线段                         |
| CircularArc2d [GetArcSegment2dAt](#Polyline8)<br />(<br />&ensp;   int index<br />) | &ensp;   <br />&ensp;   <br />段落索引<br />&ensp;           | 获取指定索引处的段落<br />的二维圆弧                         |
| LineSegment3d [GetLineSegmentAt](#Polyline9)<br />(<br />&ensp;   int index<br />) | &ensp;   <br />&ensp;   <br />段落索引<br />&ensp;           | 获取指定索引处的段落<br />的三维线段                         |
| CircularArc3d [GetArcSegmentAt](#Polyline10)<br />(<br />&ensp;   int index<br />) | &ensp;   <br />&ensp;   <br />段落索引<br />&ensp;           | 获取指定索引处的段落<br />的三维圆弧                         |
| bool [OnSegmentAt](#Polyline11)<br />(<br />&ensp;   int index,<br />&ensp;   Point2d pt2d,<br />&ensp;   double value<br />) | &ensp;   <br />&ensp;   <br />段落索引<br />二维点<br />不知道<br />&ensp; | 判断一个点是否在指定<br />索引的段落上<br />第三个参数不知道啥意思 |
| void [AddVertexAt](#Polyline12)<br />(<br />&ensp;   int index,<br />&ensp;   Point2d pt,<br />&ensp;   double bulge,<br />&ensp;   double startWidth,<br />&ensp;   double endWidth<br />) | &ensp;   <br />&ensp;   <br />节点索引<br />二维点<br />凸度<br />起始宽度<br />终止宽度<br />&ensp; | 在指定索引的节点添加一个点                                   |
| void [RemoveVertexAt](#Polyline13)<br />(<br />&ensp;   int index<br />) | &ensp;   <br />&ensp;   <br />节点索引<br />&ensp;           | 删除指定索引的节点                                           |
| Point2d [GetPoint2dAt](#Polyline14)<br />(<br />&ensp;   int index<br />) | &ensp;   <br />&ensp;   <br />节点索引<br />&ensp;           | 获取指定索引处节点的<br />二维点                             |
| void [SetPointAt](#Polyline15)<br />(<br />&ensp;   int index,<br />&ensp;   Point2d pt<br />) | &ensp;   <br />&ensp;   <br />节点索引<br />二维点<br />&ensp; | 修改指定索引处节点的<br />二维点                             |
| double [GetBulgeAt](#Polyline16)<br />(<br />&ensp;   int index<br />) | &ensp;   <br />&ensp;   <br />节点索引<br />&ensp;           | 获取指定索引处节点的<br />凸度                               |
| void [SetBulgeAt](#Polyline17)<br />(<br />&ensp;   int index,<br />&ensp;   double bulge<br />) | &ensp;   <br />&ensp;   <br />节点索引<br />凸度<br />&ensp; | 设置指定索引处节点的<br />凸度                               |
| double [GetStartWidthAt](#Polyline18)<br />(<br />&ensp;   int index<br />) | &ensp;   <br />&ensp;   <br />节点索引<br />&ensp;           | 获取指定索引处节点的<br />起始宽度                           |
| double [GetEndWidthAt](#Polyline19)<br />(<br />&ensp;   int index<br />) | &ensp;   <br />&ensp;   <br />节点索引<br />&ensp;           | 获取指定索引处节点的<br />终止宽度                           |
| void [SetStartWidthAt](#Polyline20)<br />(<br />&ensp;   int index,<br />&ensp;   double startWidth<br />) | &ensp;   <br />&ensp;   <br />节点索引<br />起始宽度<br />&ensp; | 设置指定索引处节点的<br />起始宽度                           |
| void [SetEndWidthAt](#Polyline21)<br />(<br />&ensp;   int index,<br />&ensp;   double endWidth<br />) | &ensp;   <br />&ensp;   <br />节点索引<br />终止宽度<br />&ensp; | 设置指定索引处节点的<br />终止宽度                           |
| void [MinimizeMemory](#Polyline22)()                         | 无参数                                                       | 最小化内存                                                   |
| void [MaximizeMemory](#Polyline23)()                         | 无参数                                                       | 最大化内存                                                   |
| void [Reset](#Polyline24)<br />(<br />&ensp;   bool reuse,<br />&ensp;   int vertices<br />) | 不知道                                                       | 清空多段线                                                   |

<a name="Polyline1"></a>

```c#
Polyline polyline = new Polyline();
```

<a name="Polyline2"></a>

```c#
int vertices = 5;
Polyline polyline = new Polyline(vertices);
```

<a name="Polyline3"></a>

```c#
Polyline polyline = new Polyline();
polyline.ConvertFrom(polyline2D, false);
```

<a name="Polyline4"></a>

```c#
Polyline2d polyline2D = polyline.ConvertTo(false);
```

<a name="Polyline5"></a>

```c#
List<Point3d> points = new List<Point3d>();
for (int i = 0; i < polyline.NumberOfVertices; i++)
{
    Point3d point3D = polyline.GetPoint3dAt(i);
    points.Add(point3D);
}
```

<a name="Polyline6"></a>

```c#
int index = 1;
SegmentType segmentType = polyline.GetSegmentType(index);
```

<a name="Polyline7"></a>

```c#
int index = 1;
LineSegment2d lineSegment2D = polyline.GetLineSegment2dAt(index);
```

<a name="Polyline8"></a>

```c#
int index = 1;
CircularArc2d circularArc2D = polyline.GetArcSegment2dAt(index);
```

<a name="Polyline9"></a>

```c#
int index = 1;
LineSegment3d lineSegment3D=polyline.GetLineSegmentAt(index);
```

<a name="Polyline10"></a>

```c#
int index = 1;
CircularArc3d circularArc3D=polyline.GetArcSegmentAt(index);
```

<a name="Polyline11"></a>

```c#
Point3d pt3d = polyline.GetPointAtDist(1200);
Point2d pt2d = new Point2d(pt3d.X, pt3d.Y);
double vaule = 60;
bool b = polyline.OnSegmentAt(index, pt2d, vaule);
```

<a name="Polyline12"></a>

```c#
Polyline polyline = new Polyline();
polyline.AddVertexAt(0, new Point2d(0, 0), 0, 0, 0);
polyline.AddVertexAt(1, new Point2d(10, 0), 0, 0, 0);
polyline.AddVertexAt(1, new Point2d(5, 5), 0, 2, 1);
```

<a name="Polyline13"></a>

```c#
polyline.RemoveVertexAt(1);
```

<a name="Polyline14"></a>

```c#
Point2d point2D = polyline.GetPoint2dAt(1);
```

<a name="Polyline15"></a>

```c#
Point2d point2D = new Point2d(5, 5);
polyline.SetPointAt(1, point2D);
```

<a name="Polyline16"></a>

```c#
double bulge = polyline.GetBulgeAt(0); 
```

<a name="Polyline17"></a>

```c#
polyline.SetBulgeAt(0, 2);
```

<a name="Polyline18"></a>

```c#
double startWidth=polyline.GetStartWidthAt(0);
```

<a name="Polyline19"></a>

```c#
double endWidth = polyline.GetEndWidthAt(0);
```

<a name="Polyline20"></a>

```c#
polyline.SetStartWidthAt(0, 2);
```

<a name="Polyline21"></a>

```c#
 polyline.SetEndWidthAt(0, 2);
```

<a name="Polyline22"></a>

```c#
 polyline.MinimizeMemory();
```

<a name="Polyline23"></a>

```c#
 polyline.MaximizeMemory();
```

<a name="Polyline24"></a>

```c#
polyline.Reset(true, 1); 
```

------

## 多线 Mline

https://www.bilibili.com/video/BV1xs4y1V7SM/

| 属性             | 中文         | 数据类型           | 作用                             |
| :--------------- | ------------ | ------------------ | -------------------------------- |
| NumberOfVertices | 节点数       | int                | 多线的节点数                     |
| SupressEndCaps   | 抑制终点封口 | bool               | 是否抑制终点的封口               |
| SupressStartCaps | 抑制起点封口 | bool               | 是否抑制起点的封口               |
| IsClosed         | 是否封闭     | bool               | 多线是否封闭                     |
| Normal           | 法向向量     | Vector3d           | 多线所在平面的<br />单位法向向量 |
| Scale            | 比例         | double             | 比例，两根线的间距               |
| Justification    | 对正         | MlineJustification | 对正模式                         |
| Style            | 样式         | ObjectId           | 多线的样式                       |

| 方法                                                         | 参数                                                         | 说明                                                         |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| [Mline](#Mline1)( )                                          | 无参数                                                       | 构造函数：声明一个空的多线                                   |
| void [AppendSegment](#Mline2)<br />(<br />&ensp;   Point3d newVertex<br />) | &ensp;   <br />&ensp;   <br />新的点<br />&ensp;             | 在多线末尾增加一个点                                         |
| void [RemoveLastSegment](#Mline3)<br />(<br />&ensp;   Point3d lastVertex<br />) | &ensp;   <br />&ensp;   <br />最后的点<br />&ensp;           | 移除最后一个点，参数无所谓                                   |
| void [MoveVertexAt](#Mline4)<br />(<br />&ensp;   int index,<br />&ensp;   Point3d newPosition<br />) | &ensp;   <br />&ensp;   <br />节点索引<br />新的点<br />&ensp; | 移动指定索引处的节点                                         |
| int [Element](#Mline5)<br />(<br />&ensp;   Point3d pt<br />) |                                                              | **未知方法，知道的联系我**                                   |
| Point3d [VertexAt](#Mline6)<br />(<br />&ensp;   int index<br />) | &ensp;   <br />&ensp;   <br />节点索引<br />&ensp;           | 获得指定索引处的节点                                         |
| Point3d [GetClosestPointTo](#Mline7)<br />(<br />&ensp;   Point3d givenPoint,<br />&ensp;   Vector3d normal, <br />&ensp;   bool extend,<br />&ensp;   bool excludeCaps<br />) | &ensp;   <br />&ensp;   <br />三维点<br />向量<br />是否延长<br />排除封口<br />&ensp; | 点到多线的最近点，<br />相当于取点在向量上的射线<br />与多线求最近点<br />如果延长，则可以取延长线<br />上的最近点<br />排除封口的话会忽略封口，<br />求到两根线的最近点 |
| Point3d [GetClosestPointTo](#Mline8)<br />(<br />&ensp;   Point3d givenPoint,<br />&ensp;   bool extend,<br />&ensp;   bool excludeCaps<br />) | &ensp;   <br />&ensp;   <br />三维点<br />是否延长<br />排除封口<br />&ensp; | 点到多线的最近点，<br />如果延长，则可以取延长线<br />上的最近点<br />排除封口的话会忽略封口，<br />求到两根线的最近点 |

<a name="Mline1"></a>

```c#
Database database = HostApplicationServices.WorkingDatabase;
string name = "STANDARD";
ObjectId objectId = ObjectId.Null;
using (Transaction trans = database.TransactionManager.StartTransaction())
{
    DBDictionary ss = (DBDictionary)database.MLStyleDictionaryId.GetObject(OpenMode.ForRead);
    foreach (var item in ss)
    {
        if (item.Key.ToUpper() == name)
        {
            objectId = item.Value;
        }
    }
}
Mline mline = new Mline();
mline.Style = objectId;
mline.Normal = Vector3d.ZAxis;
```

<a name="Mline2"></a>

```c#
Point3d point3D1 = new Point3d(0, 0, 0);
Point3d point3D2 = new Point3d(10, 10, 0);
mline.AppendSegment(point3D1);
mline.AppendSegment(point3D2);
```

<a name="Mline3"></a>

```c#
Point3d lastVertex = new Point3d();
mline.RemoveLastSegment(lastVertex);
```

<a name="Mline4"></a>

```c#
Point3d newPosition = new Point3d(10, 20, 0);
mline.MoveVertexAt(1, newPosition);
```

<a name="Mline5"></a>

```c#
未知。希望你的补充
```

<a name="Mline6"></a>

```c#
Point3d point3D = mline.VertexAt(1);
```

<a name="Mline7"></a>

```c#
Point3d pt = new Point3d(0, 5, 0);
Vector3d vector3D = new Vector3d(-1, -1, 0);
var po = mline.GetClosestPointTo(pt, vector3D, false, false);
```

<a name="Mline8"></a>

```c#
Point3d pt = new Point3d(0, 5, 0);
var po = mline.GetClosestPointTo(pt, false, false);
```

------

## 三维多段线 Polyline3d

https://www.bilibili.com/video/BV1YM411s7Y5/

![](png/Polyline3d.png)

| 属性     | 中文 | 数据类型   | 作用           |
| :------- | ---- | ---------- | -------------- |
| Length   | 长度 | double     | 多段线的长度   |
| PolyType | 类型 | Poly3dType | 多段线的类型   |
| Closed   | 闭合 | bool       | 多段线是否闭合 |

| 方法                                                         | 参数                                                         | 说明                                                       |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ---------------------------------------------------------- |
| [Polyline3d](#Polyline3d1)( )                                | 无参数                                                       | 构造函数：声明一条空的<br />三维多段线                     |
| [Polyline3d](#Polyline3d2)<br />(<br />&ensp;   Poly3dType type,<br />&ensp;   Point3dCollection vertices,<br />&ensp;   bool closed<br />) | &ensp;   <br />&ensp;   <br />多段线类型<br />点集<br />是否闭合<br />&ensp; | 构造函数：声明一条给定类型<br />确定点集和闭合的三维多段线 |
| void [ConvertToPolyType](#Polyline3d3)<br />(<br />&ensp;   Poly3dType newVal<br />) | &ensp;   <br />&ensp;   <br />多段线类型<br />&ensp;         | 转化多段线类型                                             |
| void [Straighten](#Polyline3d4)( )                           | 无参数                                                       | 去除多段线类型                                             |
| void [SplineFit](#Polyline3d5)<br />(<br />&ensp;   Poly3dType value,<br />&ensp;   int segments<br />) | &ensp;   <br />&ensp;   <br />多段线类型<br />段数<br />&ensp; | 修改多段线类型，设置段数，<br />段数越多，越平滑           |
| void [SplineFit](#Polyline3d6)( )                            | 无参数                                                       | 多段线类型为设置为三次                                     |
| ObjectId [AppendVertex](#Polyline3d7)<br />(<br />&ensp;   PolylineVertex3d vertexToAppend<br />) | &ensp;   <br />&ensp;   <br />多段线节点<br />&ensp;         | 给多段线增加一个点                                         |
| ObjectId [InsertVertexAt](#Polyline3d8)<br />(<br />&ensp;   ObjectId indexVertexId,<br />&ensp;   PolylineVertex3d newVertex<br />) | &ensp;   <br />&ensp;   <br />插入点的id<br />新多段线点<br />&ensp; | 在指定点插入新的点<br />（需有插入点的id）                 |
| void [InsertVertexAt](#Polyline3d9)<br />(<br />&ensp;   PolylineVertex3d indexVertex,<br />&ensp;   PolylineVertex3d newVertex<br />) | &ensp;   <br />&ensp;   <br />插入处的点<br />新多段线点<br />&ensp; | 在指定点插入新的点                                         |
| IEnumerator [GetEnumerator](#Polyline3d10)( )                | 无参数                                                       | 枚举器(不知道咋用，<br />只知道可以foreach遍历每个点)      |

<a name="Polyline3d1"></a>

```c#
Polyline3d polyline3D = new Polyline3d();
```

<a name="Polyline3d2"></a>

```c#
Poly3dType poly3DType = Poly3dType.SimplePoly;
Point3dCollection point3DCollection = new Point3dCollection();
point3DCollection.Add(new Point3d(0, 0, 0));
point3DCollection.Add(new Point3d(10, 0, 0));
point3DCollection.Add(new Point3d(10, 10, 0));
bool isClosed = true;
Polyline3d polyline3D = new Polyline3d(poly3DType, point3DCollection, isClosed);
```

<a name="Polyline3d3"></a>

```c#
Poly3dType poly3DType = Poly3dType.CubicSplinePoly;
polyline3D.ConvertToPolyType(poly3DType);
```

<a name="Polyline3d4"></a>

```c#
polyline3D.Straighten();
```

<a name="Polyline3d5"></a>

```c#
polyline3D.SplineFit(Poly3dType.QuadSplinePoly, 3);
```

<a name="Polyline3d6"></a>

```c#
polyline3D.SplineFit();
```

<a name="Polyline3d7"></a>

```c#
PolylineVertex3d polylineVertex3D = new PolylineVertex3d(new Point3d(10, 10, 0));
Database db = HostApplicationServices.WorkingDatabase;
using (Transaction trans=db.TransactionManager.StartTransaction())
{
    polyline3D = polyline3D.ObjectId.GetObject(OpenMode.ForWrite) as Polyline3d;
    polyline3D.AppendVertex(polylineVertex3D);
    trans.Commit();
}
```

<a name="Polyline3d8"></a>

```c#
Database db = HostApplicationServices.WorkingDatabase;
using (Transaction trans= db.TransactionManager.StartTransaction())
{
    polyline3D = polyline3D.ObjectId.GetObject(OpenMode.ForWrite) as Polyline3d;
    int n = 0;
    foreach (ObjectId item in polyline3D)
    {
        if (n == 1)
        {
            PolylineVertex3d polylineVertex3D = new PolylineVertex3d(new Point3d(20, 10, 0));
            polyline3D.InsertVertexAt(item, polylineVertex3D);
            break;
        }
        n++;
    }
    trans.Commit();
}
```

<a name="Polyline3d9"></a>

```c#
int n = 0;
foreach (PolylineVertex3d item in polyline3D)
{
    if (n == 1)
    {
        PolylineVertex3d polylineVertex3D = new PolylineVertex3d(new Point3d(20, 10, 0));
        polyline3D.InsertVertexAt(item, polylineVertex3D);
        break;
    }
    n++;
}
```

<a name="Polyline3d10"></a>

```c#
foreach (PolylineVertex3d item in polyline3D)
{
                
}
```











































------

## 二维多段线 Polyline2d

https://www.bilibili.com/video/BV1FL411Q7kx/

| 属性                 | 中文     | 数据类型   | 作用                                                         |
| :------------------- | -------- | ---------- | ------------------------------------------------------------ |
| Length               | 长度     | double     | 多段线的长度                                                 |
| ConstantWidth        | 全局宽度 | double     | 多段线的全局宽度                                             |
| LinetypeGenerationOn | 线型生成 | bool       | 关闭时节点会打断线型<br />打开时节点不会打断线型             |
| Elevation            | 高程     | double     | 多段线的标高（z轴）                                          |
| Normal               | 法向向量 | Vector3d   | 单位法向向量                                                 |
| Thickness            | 厚度     | double     | 多段线的厚度                                                 |
| DefaultEndWidth      | 终止宽度 | double     | 多段线默认终止宽度                                           |
| DefaultStartWidth    | 起始宽度 | double     | 多段线默认起始宽度                                           |
| Closed               | 闭合     | bool       | 多段线是否闭合                                               |
| PolyType             | 拟合方式 | Poly2dType | SimplePoly 无 <br />FitCurvePoly 曲线拟合<br />QuadSplinePoly 二次<br />CubicSplinePoly三次 |

| 方法                                                         | 参数                                                         | 说明                                               |
| ------------------------------------------------------------ | ------------------------------------------------------------ | -------------------------------------------------- |
| [Polyline2d](#Polyline2d1)( )                                | 无参数                                                       | 构造函数：声明一条空的二维多段线                   |
| [Polyline2d](#Polyline2d2)<br />(<br />&ensp;   Poly2dType type,<br />&ensp;   Point3dCollection vertices,<br />&ensp;   double elevation,<br />&ensp;   bool closed,<br />&ensp;   double startWidth,<br />&ensp;   double endWidth,<br />&ensp;   DoubleCollection bulges<br />) | &ensp;   <br />&ensp;   <br />拟合方式<br />顶点集<br />标高<br />是否闭合<br />起始宽度<br />终止宽度<br />凸度集合<br />&ensp; | 构造函数：声明一条明确属性的<br />二维多段线       |
| void [ConvertToPolyType](#Polyline2d3)<br />(<br />&ensp;   Poly2dType newVal<br />) | &ensp;   <br />&ensp;   <br />拟合方式<br />&ensp;           | 转换拟合方式                                       |
| void [Straighten](#Polyline2d4)( )                           | 无参数                                                       | 拟合方式改成SimplePoly                             |
| void [SplineFit](#Polyline2d5)<br />(<br />&ensp;   Poly2dType value,<br />&ensp;   int segments<br />) | &ensp;   <br />&ensp;   <br />拟合方式<br />段数<br />&ensp; | 修改拟合方式和段数                                 |
| void [SplineFit](#Polyline2d6)( )                            | 无参数                                                       | 拟合方式改成CubicSplinePoly                        |
| void [CurveFit](#Polyline2d7)( )                             | 无参数                                                       | 拟合方式改成FitCurvePoly                           |
| void [NonDBAppendVertex](#Polyline2d8)<br />(<br />&ensp;   Vertex2d vertexToAppend<br />) | &ensp;   <br />&ensp;   <br />节点<br />&ensp;               | 添加一个节点                                       |
| ObjectId [AppendVertex](#Polyline2d9)<br />(<br />&ensp;   Vertex2d vertexToAppend<br />) | &ensp;   <br />&ensp;   <br />节点<br />&ensp;               | 添加一个节点<br />（多段线需已经添加）             |
| ObjectId [InsertVertexAt](#Polyline2d10)<br />(<br />&ensp;   ObjectId vertexId,<br />&ensp;   Vertex2d newVertex<br />) | &ensp;   <br />&ensp;   <br />插入节点<br />新节点<br />&ensp; | 在指定节点后新加一个节点<br />（多段线需已经添加） |
| void [InsertVertexAt](#Polyline2d11)<br />(<br />&ensp;   Vertex2d indexVertex,<br />&ensp;   Vertex2d newVertex<br />) | &ensp;   <br />&ensp;   <br />插入节点<br />新节点<br />&ensp; | 在指定节点后新加一个节点                           |
| IEnumerator [GetEnumerator](#Polyline2d12)( )                | 无参数                                                       | 迭代器                                             |
| Point3d [VertexPosition](#Polyline2d13)<br />(<br />&ensp;   Vertex2d vertex<br />) | &ensp;   <br />&ensp;   <br />节点<br />&ensp;               | 获得指定节点的Point3d                              |

<a name="Polyline2d1"></a>

```c#
Polyline2d polyline2D = new Polyline2d();
```

<a name="Polyline2d2"></a>

```c#
Point3dCollection pos = new Point3dCollection
{
    new Point3d(0, 0, 0),
    new Point3d(10, 0, 0),
    new Point3d(10, 10, 0),
    new Point3d(20, 10, 0)
};
DoubleCollection doubles = new DoubleCollection
{
    0,
    0,
    0,
    0
};
Polyline2d polyline2D = new Polyline2d(Poly2dType.SimplePoly, pos, 0, false, 0, 0, doubles);

```

<a name="Polyline2d3"></a>

```c#
polyline2D.ConvertToPolyType(Poly2dType.FitCurvePoly);
```

<a name="Polyline2d4"></a>

```c#
polyline2D.Straighten();
```

<a name="Polyline2d5"></a>

```c#
polyline2D.SplineFit(Poly2dType.CubicSplinePoly, 100);
```

<a name="Polyline2d6"></a>

```c#
polyline2D.SplineFit();
```

<a name="Polyline2d7"></a>

```c#
polyline2D.CurveFit();
```

<a name="Polyline2d8"></a>

```c#
Vertex2d vertex2D = new Vertex2d(new Point3d(20, 20, 0), 0, 0, 0, 0);
polyline2D.NonDBAppendVertex(vertex2D);
```

<a name="Polyline2d9"></a>

```c#
Vertex2d vertex2D = new Vertex2d(new Point3d(20, 20, 0), 0, 0, 0, 0);
Database db = HostApplicationServices.WorkingDatabase;
using (Transaction trans = db.TransactionManager.StartTransaction())
{
    polyline2D.ObjectId.GetObject(OpenMode.ForWrite);
    polyline2D.AppendVertex(vertex2D);
    trans.Commit();
}
```

<a name="Polyline2d10"></a>

```c#
Database db = HostApplicationServices.WorkingDatabase;
using (Transaction trans = db.TransactionManager.StartTransaction())
{
    polyline2D.ObjectId.GetObject(OpenMode.ForWrite);
    int n = 0;
    foreach (ObjectId item in polyline2D)
    {
        if (n == 1)
        {
            Vertex2d vertex2D = new Vertex2d(new Point3d(15, 5, 0), 0, 0, 0, 0);
            polyline2D.InsertVertexAt(item, vertex2D);
            break;
        }
        n++;
    }
    trans.Commit();
}
```

<a name="Polyline2d11"></a>

```c#
int n = 0;
foreach (Vertex2d item in polyline2D)
{
    if (n == 1)
    {
        Vertex2d vertex2D = new Vertex2d(new Point3d(15, 8, 0), 0, 0, 0, 0);
        polyline2D.InsertVertexAt(item, vertex2D);
        break;
    }
    n++;
}
```

<a name="Polyline2d12"></a>

```c#
//IEnumerator enumerator = polyline2D.GetEnumerator();
//var obj = enumerator.Current;
//enumerator.MoveNext();
foreach (var item in polyline2D)
{

}
```

<a name="Polyline2d13"></a>

```c#
foreach (Vertex2d item in polyline2D)
{
    Point3d point3D = polyline2D.VertexPosition(item);
}
```

------

## 单行文字DBText

https://www.bilibili.com/video/BV1bg4y1s7eH/

| 属性               | 中文                     | 数据类型           | 作用                                       |
| ------------------ | ------------------------ | ------------------ | ------------------------------------------ |
| Justify            | [对齐点](#DBText1)       | AttachmentPoint    | 对齐方式                                   |
| VerticalMode       | [垂直对齐方式](#DBText2) | TextVerticalMode   | 垂直对齐方式                               |
| HorizontalMode     | [水平对齐方式](#DBText3) | TextHorizontalMode | 水平对齐方式                               |
| IsMirroredInY      | [Y轴镜像](#DBText4)      | bool               | 是否延Y轴镜像                              |
| IsMirroredInX      | [X轴镜像](#DBText5)      | bool               | 是否延X轴镜像                              |
| TextStyleId        | 文字样式Id               | ObjectId           | 文字样式的ObjectId                         |
| TextStyleName      | 文字样式名称             | string             | 文字样式的名称                             |
| TextString         | 文字内容                 | string             | 单行文字的内容                             |
| WidthFactor        | 宽度系数                 | double             | 宽度系数，1为默认                          |
| Height             | 字高                     | double             | 字体的高度                                 |
| Rotation           | [旋转弧度](#DBText6)     | double             | 旋转的弧度(逆时针)                         |
| Oblique            | [倾斜弧度](#DBText7)     | double             | 倾斜的弧度(左负右正)                       |
| Thickness          | 厚度                     | double             | 厚度                                       |
| Normal             | 平面法向向量             | Vector3d           | 所在平面法向向量                           |
| IsDefaultAlignment | 是否默认对齐             | bool               | 否代表有对齐点                             |
| AlignmentPoint     | 文本对齐点               | Point3d            | 设置某些对齐方式后<br />的对齐点，代替基点 |
| Position           | 基点位置                 | Point3d            | 原始的基点                                 |

  <a name="DBText1"></a>

![ ](png/DBText1.png)

<a name="DBText2"></a>

![ ](png/DBText2.png)

<a name="DBText3"></a>

![ ](png/DBText3.png)

<a name="DBText4"></a>

![ ](png/DBText4.png)

<a name="DBText5"></a>

![ ](png/DBText5.png)

<a name="DBText6"></a>

![ ](png/DBText6.png)

<a name="DBText7"></a>

![ ](png/DBText7.png)

| 方法                                                         | 参数                                           | 说明                   |
| ------------------------------------------------------------ | ---------------------------------------------- | ---------------------- |
| [DBText( )](#DBText8)                                        | 无参数                                         | 构造函数               |
| int [CorrectSpelling( )](#DBText9)                           | 无参数                                         | 拼写错误？(不知道咋用) |
| void [AdjustAlignment](#DBText10)<br />(<br />&ensp;   Database alternateDatabaseToUse<br />) | &ensp;   <br />&ensp;   <br />试试<br />&ensp; | 不懂                   |
| void [ConvertFieldToText( )](#DBText11)                      | 无参数                                         | 不懂                   |
| string [getTextWithFieldCodes( )](#DBText12)                 | 无参数                                         | 不懂                   |

<a name="DBText8"></a>

```c#
DBText dBText = new DBText();
```

<a name="DBText9"></a>

```c#

```

<a name="DBText10"></a>

```c#

```

<a name="DBText11"></a>

```c#

```

<a name="DBText12"></a>

```c#

```



## 构造线Xline

| 属性        | 数据类型 | 说明                                             |
| ----------- | -------- | ------------------------------------------------ |
| BasePoint   | Point3d  | 构造线的基准点                                   |
| SecondPoint | Point3d  | 构造线的第二个点，和基准点的向量就是构造线的放心 |
| UnitDir     | Vector3d | 构造线的向量                                     |

| 方法                 | 参数   | 说明     |
| -------------------- | ------ | -------- |
| [ Xline( )](#Xline1) | 无参数 | 构造函数 |

<a name="Xline1"></a>

```c#
Xline xline = new Xline();
xline.BasePoint = new Point3d();
xline.UnitDir = new Vector3d(10, 10, 0);
```



## 射线Ray

| 属性        | 数据类型 | 说明                                             |
| ----------- | -------- | ------------------------------------------------ |
| BasePoint   | Point3d  | 构造线的基准点                                   |
| SecondPoint | Point3d  | 构造线的第二个点，和基准点的向量就是构造线的放心 |
| UnitDir     | Vector3d | 构造线的向量                                     |

| 方法             | 参数   | 说明     |
| ---------------- | ------ | -------- |
| [ Ray( )](#Ray1) | 无参数 | 构造函数 |

<a name="Ray1"></a>

```c#
Ray ray = new Ray();
ray.BasePoint = new Point3d();
ray.UnitDir = new Vector3d(10, 10, 0);
```

扩展一下，已知一点，求在指定方向最近的有交点的线

```
Point3d point = new Point3d();
Vector3d vector = new Vector3d(1, 1, 0);
List<Curve> curves = SelectEntities<Curve>();
if (curves.Count == 0) return;
Curve curve = GetClosestCurve(point, vector, curves);
if (curve == null) return;
Curve curve1 = curve.Clone() as Curve;
curve1.ColorIndex = 2;
curve1.AddEntity();
```

```c#
private static Curve GetClosestCurve(Point3d point, Vector3d vector, List<Curve> curves)
{
    Ray ray = new Ray();
    ray.BasePoint = point;
    ray.UnitDir = vector;
    Point3d closestPoint = new Point3d();
    double closestDis = double.MaxValue;
    Curve closestCur = null;
    foreach (var curve in curves)
    {
        Point3dCollection pos = new Point3dCollection();
        ray.IntersectWith(curve, Intersect.OnBothOperands, pos, IntPtr.Zero, IntPtr.Zero);
        foreach (Point3d po in pos)
        {
            double dis = po.DistanceTo(point);
            if (dis < closestDis)
            {
                closestPoint = po;
                closestDis = dis;
                closestCur = curve;
            }
        }
    }
    return closestCur;
}
```



------

# 标注Dimension

## 转角标注RotatedDimension

https://www.bilibili.com/video/BV1is4y1J7n4/

![](png/RotatedDimension.png)

| 属性         | 中文         | 数据类型 | 作用                 |
| ------------ | ------------ | -------- | -------------------- |
| Rotation     | 文字倾斜     | double   | 文字所在的线的切斜角 |
| Oblique      | 标注转角     | double   | 两个标注线的倾斜角   |
| DimLinePoint | 标注线定位点 | Point3d  | 标注线经过的点       |
| XLine2Point  | 标注终点     | Point3d  | 标注终点所在的点     |
| XLine1Point  | 标注起点     | Point3d  | 标注起点所在的点     |

| 方法                                                         | 参数                                                         | 说明     |
| ------------------------------------------------------------ | ------------------------------------------------------------ | -------- |
| RotatedDimension( )                                          | 无参数                                                       | 构造函数 |
| RotatedDimension<br />(<br />&ensp;   double rotation,<br />&ensp;   Point3d line1Point,<br />&ensp;   Point3d line2Point,<br />&ensp;   Point3d dimensionLinePoint,<br />&ensp;   string dimensionText,<br />&ensp;   ObjectId dimensionStyle<br />) | &ensp;   <br />&ensp;   <br />倾斜角<br />标注起点<br />标注终点<br />标注线定位点<br />标注文字替换<br />标注样式<br />&ensp; | 构造函数 |

```c#
RotatedDimension rotatedDimension = new RotatedDimension();
```

```
double rotation = Math.PI / 6;
Point3d line1Point = new Point3d(0, 0, 0);
Point3d line2Point = new Point3d(100, 0, 0);
Point3d dimensionLinePoint = new Point3d(50, 30, 0);
string dimensionText = "150";
Document doc = Application.DocumentManager.MdiActiveDocument;
ObjectId dimensionStyle = doc.Database.DimStyleTableId;
RotatedDimension rotatedDimension = new RotatedDimension(rotation, line1Point, line2Point, dimensionLinePoint,dimensionText, dimensionStyle);
```

------

## 对齐标注AlignedDimension

https://www.bilibili.com/video/BV1kM4y1U7jR/

![](png/AlignedDimension.png)

| 属性         | 中文         | 数据类型 | 作用               |
| ------------ | ------------ | -------- | ------------------ |
| Oblique      | 标注转角     | double   | 两个标注线的倾斜角 |
| DimLinePoint | 标注线定位点 | Point3d  | 标注线经过的点     |
| XLine2Point  | 标注点2      | Point3d  | 标注终点所在的点   |
| XLine1Point  | 标注点1      | Point3d  | 标注起点所在的点   |

| 方法                                                         | 参数                                                         | 说明     |
| ------------------------------------------------------------ | ------------------------------------------------------------ | -------- |
| AlignedDimension( )                                          | 无参数                                                       | 构造函数 |
| AlignedDimension<br />(<br />&ensp;   Point3d line1Point,<br />&ensp;   Point3d line2Point,<br />&ensp;   Point3d dimensionLinePoint,<br />&ensp;   string dimensionText,<br />&ensp;   ObjectId dimensionStyle<br />) | &ensp;   <br />&ensp;   <br />标注起点<br />标注终点<br />标注线定位点<br />标注文字替换<br />标注样式<br />&ensp; | 构造函数 |

```c#
AlignedDimension alignedDimension = new AlignedDimension();
```

```c#
Point3d line1Point = new Point3d(0, 0, 0);
Point3d line2Point = new Point3d(100, 0, 0);
Point3d dimensionLinePoint = new Point3d(50, 30, 0);
string dimensionText = "";
Document doc = Application.DocumentManager.MdiActiveDocument;
ObjectId dimensionStyle = doc.Database.DimStyleTableId;
AlignedDimension alignedDimension = new AlignedDimension(line1Point, line2Point, dimensionLinePoint, dimensionText,
dimensionStyle);
```

------

## 角度标注LineAngularDimension2

https://www.bilibili.com/video/BV1DL411X7uy/

![](png/LineAngularDimension2.png)

| 属性        | 中文         | 数据类型 | 作用         |
| ----------- | ------------ | -------- | ------------ |
| XLine2End   | 标注线2终点  | Point3d  | 标注线2终点  |
| XLine2Start | 标注线2起点  | Point3d  | 标注线2起点  |
| XLine1End   | 标注线1终点  | Point3d  | 标注线1终点  |
| XLine1Start | 标注线1起点  | Point3d  | 标注线1起点  |
| ArcPoint    | 标注线定位点 | Point3d  | 标注线定位点 |

| 方法                                                         | 参数                                                         | 说明     |
| ------------------------------------------------------------ | ------------------------------------------------------------ | -------- |
| LineAngularDimension2( )                                     | 无参数                                                       | 构造函数 |
| LineAngularDimension2<br />(<br />&ensp;   Point3d line1Start,<br />&ensp;   Point3d line1End,<br />&ensp;   Point3d line2Start,<br />&ensp;   Point3d line2End,<br />&ensp;   Point3d arcPoint,<br />&ensp;   string dimensionText,<br />&ensp;   ObjectId dimensionStyle<br />) | &ensp;   <br />&ensp;   <br />标注线1起点<br />标注线1终点<br />标注线2起点<br />标注线2终点<br />标注线定位点<br />文字替换<br />标注样式<br />&ensp; | 构造函数 |

```c#
LineAngularDimension2 lineAngularDimension2 = new LineAngularDimension2();
```

```c#
Point3d line1Start = new Point3d(0, 0, 0);
Point3d line1End = new Point3d(0, 50, 0);
Point3d line2Start = new Point3d(10, 0, 0);
Point3d line2End = new Point3d(50, 50, 0);
Point3d arcPoint = new Point3d(30, 80, 0);
string dimensionText = "";
Document doc = Application.DocumentManager.MdiActiveDocument;
ObjectId dimensionStyle = doc.Database.DimStyleTableId;
LineAngularDimension2 lineAngularDimension2 = new LineAngularDimension2(line1Start,line1End,line2Start,line2End, arcPoint,
dimensionText, dimensionStyle);
```

------

## 半径标注RadialDimension

https://www.bilibili.com/video/BV1Dg4y137Nu/

![](png/RadialDimension1.png)

![](png/RadialDimension2.png)

| 属性         | 中文         | 数据类型 | 作用         |
| ------------ | ------------ | -------- | ------------ |
| ChordPoint   | 标注线定位点 | Point3d  | 标注线定位点 |
| Center       | 圆心         | Point3d  | 圆心         |
| LeaderLength | 引线长度     | double   | 引线长度     |

| 方法                                                         | 参数                                                         | 说明     |
| ------------------------------------------------------------ | ------------------------------------------------------------ | -------- |
| RadialDimension( )                                           | 无参数                                                       | 构造函数 |
| RadialDimension<br />(<br />&ensp;   Point3d center,<br />&ensp;   Point3d chordPoint,<br />&ensp;   double leaderLength,<br />&ensp;   string dimensionText,<br />&ensp;   ObjectId dimensionStyle<br />) | &ensp;   <br />&ensp;   <br />原点<br />标注线定位点<br />引线长度<br />文字替换<br />标注样式<br />&ensp; | 构造函数 |

```c#
RadialDimension radialDimension = new RadialDimension();
```

```c#
Point3d center = new Point3d(0, 0, 0);
Point3d chordPoint = new Point3d(50, 50, 0);
double leaderLength = 100;
string dimensionText = "";
Document doc = Application.DocumentManager.MdiActiveDocument;
ObjectId dimensionStyle = doc.Database.DimStyleTableId;
RadialDimension radialDimension = new RadialDimension(center, chordPoint, leaderLength, dimensionText,
dimensionStyle);
AddEntity(radialDimension);
```

------

## 直径标注DiametricDimension

https://www.bilibili.com/video/BV14s4y1D7Ar/

![](png/DiametricDimension1.png)

![](png/DiametricDimension2.png)

| 属性          | 中文     | 数据类型 | 作用     |
| ------------- | -------- | -------- | -------- |
| FarChordPoint | 远弦点   | Point3d  | 远弦点   |
| ChordPoint    | 近弦点   | Point3d  | 近弦点   |
| LeaderLength  | 引线长度 | double   | 引线长度 |

| 方法                                                         | 参数                                                         | 说明     |
| ------------------------------------------------------------ | ------------------------------------------------------------ | -------- |
| DiametricDimension( )                                        | 无参数                                                       | 构造函数 |
| DiametricDimension<br />(<br />&ensp;   Point3d chordPoint,<br />&ensp;   Point3d farChordPoint,<br />&ensp;   double leaderLength,<br />&ensp;   string dimensionText,<br />&ensp;   ObjectId dimensionStyle<br />) | &ensp;   <br />&ensp;   <br />近弦点<br />远弦点<br />引线长度<br />文字替换<br />标注样式<br />&ensp; | 构造函数 |

```c#
DiametricDimension diametricDimension = new DiametricDimension();
```

```
Point3d chordPoint = new Point3d(50, 0, 0);
Point3d farChordPoint = new Point3d(-50, 0, 0);
double leaderLength = 20;
string dimensionText = "";
Document doc = Application.DocumentManager.MdiActiveDocument;
ObjectId dimensionStyle = doc.Database.DimStyleTableId;
DiametricDimension diametricDimension = new DiametricDimension(chordPoint, farChordPoint,leaderLength,
dimensionText,dimensionStyle);
AddEntity(diametricDimension);
```

------

## 弧长标注ArcDimension

https://www.bilibili.com/video/BV1v24y1j7o6/

| 属性          | 数据类型 | 说明                                                         |
| ------------- | -------- | ------------------------------------------------------------ |
| HasLeader     | bool     | 是否有说明引线                                               |
| IsPartial     | bool     | 是否是局部的                                                 |
| ArcSymbolType | int      | 圆弧样式（默认0）<br />0=弧长符号在文字前<br />1=弧长符号在文字上<br />2=不显示弧长符号<br />但是我怎么改他都不变<br />不知道为啥 |
| XLine2Point   | Point3d  | 标注终点                                                     |
| XLine1Point   | Point3d  | 标注起点                                                     |
| Leader2Point  | Point3d  | 额外点2                                                      |
| Leader1Point  | Point3d  | 额外点1                                                      |
| CenterPoint   | Point3d  | 圆心                                                         |
| ArcPoint      | Point3d  | 标注定位点                                                   |
| ArcEndParam   | double   | 圆弧终点参数                                                 |
| ArcStartParam | double   | 圆弧起点参数                                                 |

| 方法                                                         | 参数                                                         | 说明     |
| ------------------------------------------------------------ | ------------------------------------------------------------ | -------- |
| ArcDimension<br />(<br />&ensp;   Point3d centerPoint,<br />&ensp;   Point3d xLine1Point,<br />&ensp;   Point3d xLine2Point,<br />&ensp;   Point3d arcPoint,<br />&ensp;   string dimensionText,<br />&ensp;   ObjectId dimensionStyle<br />) | &ensp;   <br />&ensp;   <br />圆心<br />标注起点<br />标注终点<br />标注线定位点<br />文字替代<br />标注样式<br />&ensp; | 构造函数 |

```c#
Point3d centerPoint = new Point3d(0, 0, 0);
Point3d xLine1Point = new Point3d(0, 50, 0);
Point3d xLine2Point = new Point3d(50, 0, 0);
Point3d arcPoint = new Point3d(70, 70, 0);
string dimensionText = "";
Document doc = Application.DocumentManager.MdiActiveDocument;
ObjectId dimensionStyle = doc.Database.DimStyleTableId;
ArcDimension arcDimension = new ArcDimension(centerPoint, xLine1Point, xLine2Point, arcPoint, dimensionText,
dimensionStyle);
AddEntity(arcDimension);
```

------

## 折弯标注RadialDimensionLarge

https://www.bilibili.com/video/BV1f84y1M7fj/

| 属性           | 数据类型 | 说明                                     |
| -------------- | -------- | ---------------------------------------- |
| OverrideCenter | Point3d  | 中心位置代替点                           |
| JogPoint       | Point3d  | 折弯坐标                                 |
| ChordPoint     | Point3d  | 标记定位点                               |
| Center         | Point3d  | 圆心                                     |
| JogAngle       | double   | 折弯角度（设置了无效<br />不知道为啥子） |

| 方法                                                         | 参数                                                         | 说明     |
| ------------------------------------------------------------ | ------------------------------------------------------------ | -------- |
| RadialDimensionLarge( )                                      | 无参数                                                       | 构造函数 |
| RadialDimensionLarge<br />(<br />&ensp;   Point3d center,<br />&ensp;   Point3d chordPoint,<br />&ensp;   Point3d overrideCenter,<br />&ensp;   Point3d jogPoint,<br />&ensp;   double jogAngle,<br />&ensp;   string dimensionText,<br />&ensp;   ObjectId dimensionStyle<br />) | &ensp;   <br />&ensp;   <br />圆心<br />标注定位点<br />中心位置代替点<br />折弯坐标<br />标注样式<br />文字替代<br />标注样式<br />&ensp; | 构造函数 |

------

## 坐标标注OrdinateDimension

![](png/OrdinateDimension.png)

| 属性           | 数据类型 | 说明          |
| -------------- | -------- | ------------- |
| LeaderEndPoint | Point3d  | 引线终点      |
| DefiningPoint  | Point3d  | 标注点        |
| Origin         | Point3d  | 原点          |
| UsingXAxis     | bool     | 以X坐标为计量 |
| UsingYAxis     | bool     | 以Y坐标为计量 |

| 方法                                                         | 参数                                                         | 说明     |
| ------------------------------------------------------------ | ------------------------------------------------------------ | -------- |
| OrdinateDimension( )                                         | 无参数                                                       | 构造函数 |
| OrdinateDimension<br />(<br />&ensp;   bool useXAxis,<br />&ensp;   Point3d definingPoint,<br />&ensp;   Point3d leaderEndPoint,<br />&ensp;   string dimText,<br />&ensp;   ObjectId dimStyle<br />) | &ensp;   <br />&ensp;   <br />以X坐标为计量<br />标注点<br />引线终点<br />文字替换<br />标注样式<br />&ensp; | 构造函数 |

```c#
OrdinateDimension ordinateDimension = new OrdinateDimension();
```

```c#
bool useXAxis = true;
Point3d definingPoint = new Point3d(50, 20, 0);
Point3d leaderEndPoint = new Point3d(70, 50, 0);
string dimensionText = "";
Document doc = Application.DocumentManager.MdiActiveDocument;
ObjectId dimensionStyle = doc.Database.DimStyleTableId;
OrdinateDimension ordinateDimension = new OrdinateDimension(useXAxis, definingPoint, leaderEndPoint,
dimensionText,dimensionStyle);
```

------

# GE曲线Curve3d

## 线段LineSegment3d

| 属性       | 数据类型 | 说明 |
| ---------- | -------- | ---- |
| Length     | double   | 长度 |
| StartPoint | Point3d  | 起点 |
| MidPoint   | Point3d  | 中点 |
| EndPoint   | Point3d  | 终点 |

| 方法                                                         | 参数                                                         | 说明                                                         |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| [LineSegment3d](#LineSegment3d1)( )                          | 无参数                                                       | 构造函数                                                     |
| [LineSegment3d](#LineSegment3d2)<br/>(<br/>&ensp;   Point3d point1,<br/>&ensp;   Point3d point2<br/>) | &ensp;   <br />&ensp;   <br />起点<br />终点<br />&ensp;     | 构造函数                                                     |
| [LineSegment3d](#LineSegment3d3)<br/>(<br/>&ensp;   Point3d point,<br/>&ensp;   Vector3d vector<br/>) | &ensp;   <br />&ensp;   <br />起点<br />起点到终点的向量<br />&ensp; | 构造函数                                                     |
| Plane [GetBisector](#LineSegment3d4)( )                      | 无参数                                                       | 获取通过中点，并且垂直线的平面                               |
| Point3d [BaryComb](#LineSegment3d5)<br/>(<br/>&ensp;   double blendCoefficient<br/>) | &ensp;   <br />&ensp;   <br />系数<br />&ensp;               | 0起点，1终点，(0,1)在线上<br /><0或者>1在延长线上            |
| void [Set](#LineSegment3d6)<br/>(<br/>&ensp;   Point3d point1,<br/>&ensp;   Point3d point2<br/>) | &ensp;   <br />&ensp;   <br />起点<br />终点<br />&ensp;     | 设置线段的起点终点                                           |
| void [Set](#LineSegment3d7)<br/>(<br/>&ensp;   Point3d point,<br/>&ensp;   Vector3d vector<br/>) | &ensp;   <br />&ensp;   <br />起点<br />起点到终点向量<br />&ensp; | 设置线段起点和<br />起点到终点的向量                         |
| void [Set](#LineSegment3d8)<br/>(<br/>&ensp;   Curve3d curve,<br/>&ensp;   Point3d point,<br/>&ensp;   double parameter<br/>) | &ensp;   <br />&ensp;   <br />一个ge曲线<br />点<br />猜测参数<br />&ensp; | 一个平面曲线，<br />曲线平面上的一个点<br />切点猜测参数，<br />会得到通过这个点，<br />与曲线相切的线 |
| void [Set](#LineSegment3d9)<br/>(<br/>&ensp;   Curve3d curve1,<br/>&ensp;   Curve3d curve2,<br/>&ensp;   double parameter1,<br/>&ensp;   double parameter2<br/>) | &ensp;   <br />&ensp;   <br />ge曲线1<br />ge曲线2<br />曲线1猜测参数<br />曲线2猜测参数<br />&ensp; | 求两个同一平面的曲线的切线                                   |

<a name="LineSegment3d1"></a>

```c#
LineSegment3d line = new LineSegment3d();
```

<a name="LineSegment3d2"></a>

```c#
Point3d point1 = new Point3d(0, 0, 0);
Point3d point2 = new Point3d(10, 0, 0);
LineSegment3d line = new LineSegment3d(point1, point2);
```

<a name="LineSegment3d3"></a>

```c#
Point3d point = new Point3d(0, 0, 0);
Vector3d vector = new Vector3d(10, 0, 0);
LineSegment3d line = new LineSegment3d(point, vector);
```

<a name="LineSegment3d4"></a>

```c#
Point3d point = new Point3d(0, 0, 0);
Vector3d vector = new Vector3d(0, 0, 10);
LineSegment3d line = new LineSegment3d(point, vector);
Plane plane = line.GetBisector();
```

<a name="LineSegment3d5"></a>

```c#
Point3d point = new Point3d(0, 0, 0);
Vector3d vector = new Vector3d(10, 0, 0);
LineSegment3d line = new LineSegment3d(point, vector);
Point3d point1 = line.BaryComb(0.6);//(6,0,0)
Point3d point2 = line.BaryComb(-2);//(-20,0,0)
Point3d point3 = line.BaryComb(3);//(30,0,0)
```

<a name="LineSegment3d6"></a>

```c#
Point3d point = new Point3d(0, 0, 0);
Vector3d vector = new Vector3d(10, 0, 0);
LineSegment3d line = new LineSegment3d(point, vector);
Point3d point1 = new Point3d(5, 5, 0);
Point3d point2 = new Point3d(10, 10, 0);
line.Set(point1, point2);
```

<a name="LineSegment3d7"></a>

```c#
Point3d point = new Point3d(0, 0, 0);
Vector3d vector = new Vector3d(10, 0, 0);
LineSegment3d line = new LineSegment3d(point, vector);
Point3d point1 = new Point3d(5, 5, 0);
Vector3d vector1 = new Vector3d(5, 5, 0);
line.Set(point1, vector1);
```

<a name="LineSegment3d8"></a>

```c#
Circle circle = new Circle(new Point3d(), Vector3d.ZAxis, 10);
Curve3d curve = circle.GetGeCurve();
Point3d point = new Point3d(20, 0, 0);
double parameter = 1;
LineSegment3d line = new LineSegment3d();
line.Set(curve, point, parameter);
```

```c#
//扩展一下。求点到圆的两个切线
Circle circle = new Circle(new Point3d(), Vector3d.ZAxis, 10);
Curve3d curve = circle.GetGeCurve();
Point3d point = new Point3d(20, 0, 0);
List<LineSegment3d> lines = new List<LineSegment3d>();
for (double i = 0; i < Math.PI * 2; i += 0.1)
{
    LineSegment3d line = new LineSegment3d();
    line.Set(curve, point, i);
    if (lines.Any(l => l.EndPoint == line.EndPoint)) continue;
    lines.Add(line);
}
```



<a name="LineSegment3d9"></a>

```c#
Circle circle1 = new Circle(new Point3d(), Vector3d.ZAxis, 10);
Circle circle2 = new Circle(new Point3d(50,0,0), Vector3d.ZAxis, 20);
Curve3d curve1 = circle1.GetGeCurve();
Curve3d curve2 = circle2.GetGeCurve();
double parameter1 = 1.5;
double parameter2 = 1.5;
LineSegment3d line = new LineSegment3d();
line.Set(curve1, curve2, parameter1, parameter2);
```

```c#
//扩展一下。求两个圆的四个切线
Circle circle1 = new Circle(new Point3d(), Vector3d.ZAxis, 10);
Circle circle2 = new Circle(new Point3d(50, 0, 0), Vector3d.ZAxis, 20);
Curve3d curve1 = circle1.GetGeCurve();
Curve3d curve2 = circle2.GetGeCurve();
List<LineSegment3d> lines = new List<LineSegment3d>();
for (double i = 0; i < Math.PI*2; i += 0.1)
{
    for (double j = 0; j < Math.PI * 2; j += 0.1)
    {
        LineSegment3d line = new LineSegment3d();
        line.Set(curve1, curve2, i, j);
        if (lines.Any(l => l.StartPoint == line.StartPoint || l.EndPoint == line.EndPoint)) continue;
        lines.Add(line);
    }
}
lines.ForEach(l => Curve.CreateFromGeCurve(l).AddEntity());
circle1.AddEntity();
circle2.AddEntity();
```

------

# 三维向量Vector3d

| 属性           | 数据类型 | 说明                  |
| -------------- | -------- | --------------------- |
| LargestElement | int      | 最长的项的序列(0,1,2) |
| LengthSqrd     | double   | 长度的平方x²+y²+z²    |
| Length         | double   | 长度√(x² + y² + z²)   |
| this[int i]    | double   | 按序号取值0-X 1-Y 2-Z |
| XAxis          | Vector3d | X轴单位向量           |
| YAxis          | Vector3d | Y轴单位向量           |
| ZAxis          | Vector3d | Z轴单位向量           |
| X              | double   | 向量的X值             |
| Y              | double   | 向量的Y值             |
| Z              | double   | 向量的Z值             |

| Vector3d RotateBy<br/>(<br/>&ensp;  double angle,<br/>&ensp;  Vector3d axis<br />) | &ensp;   <br />&ensp;   <br />起点<br />起点到终点向量<br />&ensp; | 111  |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ---- |

```c#

```

