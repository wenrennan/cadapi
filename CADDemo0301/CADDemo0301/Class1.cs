﻿using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.DatabaseServices.Filters;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.Internal;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.Windows;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.ConstrainedExecution;
using System.Runtime.ExceptionServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Schema;
using static Autodesk.AutoCAD.Windows.SaveFileDialog;

namespace CADDemo0301
{
    public static class Class1
    {
        [CommandMethod("GetLine")]
        public static void GetLine()
        {
            Entity entity = GetEntity();
            if (entity is Line)
            {
                Line line = entity as Line;
                double lenght = line.Length;
                double angle = line.Angle;
                Vector3d delta = line.Delta;
                Vector3d normal = line.Normal;
                double thickness = line.Thickness;
                Point3d startPoint = line.StartPoint;
                Point3d endPoint = line.EndPoint;
            }
            Database db = HostApplicationServices.WorkingDatabase;
            using (Transaction trans = db.TransactionManager.StartTransaction())
            {
                LayerTable lt = (LayerTable)db.LayerTableId.GetObject(OpenMode.ForRead);
                foreach (ObjectId item in lt)
                {
                    //LayerTableRecord ltr=(LayerTableRecord)
                }
            }

        }

        [CommandMethod("AddCircle")]
        public static void AddCircle()
        {

            Point3d center = new Point3d(10, 10, 0);
            Vector3d normal = Vector3d.ZAxis;
            double radius = 10;
            Circle circle = new Circle(center, normal, radius);
            AddEntity(circle);
        }
        [CommandMethod("AddArc")]
        public static void AddArc()
        {
            Point3d center = Point3d.Origin;
            Vector3d normal = -Vector3d.ZAxis;
            double radius = 5;
            double startAngle = 0;
            double endAngle = Math.PI / 3;
            Arc arc = new Arc(center, normal, radius, startAngle, endAngle);
            AddEntity(arc);
        }
        [CommandMethod("AddEllipse")]
        public static void AddEllipse()
        {
            Point3d center = new Point3d(0, 0, 0);
            Vector3d unitNormal = Vector3d.ZAxis;
            Vector3d majorAxis = new Vector3d(10, 10, 0);
            double radiusRatio = 0.5;
            double startAngle = Math.PI / 4;
            double endAngle = Math.PI;
            Ellipse ellipse = new Ellipse(center, unitNormal, majorAxis, radiusRatio, startAngle, endAngle);
            AddEntity(ellipse);
        }
        [CommandMethod("AddPolyline")]
        public static void AddPolyline()
        {
            Polyline polyline = new Polyline();
            polyline.AddVertexAt(0, new Point2d(0, 0), 0, 0, 0);
            polyline.AddVertexAt(1, new Point2d(10, 0), 0, 0, 0);
            polyline.AddVertexAt(2, new Point2d(5, 5), 0, 0, 0);
            polyline.SetPointAt(1, new Point2d(15, 0));
            polyline.Reset(true, 1);
            AddEntity(polyline);
            //Polyline2d polyline2D = polyline.ConvertTo(false);
            //AddEntity(polyline2D);
            //Polyline polyline = GetEntity() as Polyline;
            //double bulge = polyline.GetBulgeAt(0);
            //;
            //List<Point3d> points = new List<Point3d>();
            //for (int i = 0; i < polyline.NumberOfVertices; i++)
            //{
            //    Point3d point3D = polyline.GetPoint3dAt(i);
            //    points.Add(point3D);
            //}
            //int index = 1;
            //SegmentType segmentType = polyline.GetSegmentType(index);
            //int index = 1;
            //LineSegment2d lineSegment2D = polyline.GetLineSegment2dAt(index);
            //int index = 1;
            //CircularArc2d circularArc2D = polyline.GetArcSegment2dAt(index);
            //Point3d pt3d = polyline.GetPointAtDist(5);
            //Point2d pt2d = new Point2d(8, 2);
            //double vaule = 60;
            //bool b = polyline.OnSegmentAt(1, pt2d, vaule);
        }
        [CommandMethod("AddMline")]
        public static void AddMline()
        {
            Database database = HostApplicationServices.WorkingDatabase;
            string name = "STANDARD";
            ObjectId objectId = ObjectId.Null;
            using (Transaction trans = database.TransactionManager.StartTransaction())
            {
                DBDictionary ss = (DBDictionary)database.MLStyleDictionaryId.GetObject(OpenMode.ForRead);
                foreach (var item in ss)
                {
                    if (item.Key.ToUpper() == name)
                    {
                        objectId = item.Value;
                    }
                }
            }
            Mline mline = new Mline();

            mline.Style = objectId;
            mline.Normal = Vector3d.ZAxis;
            mline.Justification = MlineJustification.Zero;
            Point3d point3D1 = new Point3d(0, 0, 0);
            Point3d point3D2 = new Point3d(0, 10, 0);
            mline.AppendSegment(point3D1);
            mline.AppendSegment(point3D2);
            //mline.SupressStartCaps= true;
            //mline.SupressEndCaps= true;
            //Point3d lastVertex = new Point3d();
            //mline.RemoveLastSegment(lastVertex);
            //Point3d newPosition = new Point3d(10, 20, 0);
            //mline.MoveVertexAt(2, newPosition);
            //Point3d pt = new Point3d(0, 0, 0);
            //var po = mline.GetClosestPointTo(pt, true, true);
            Point3d pt = new Point3d(0, 5, 0);
            Vector3d vector3D = new Vector3d(-1, -1, 0);
            var po = mline.GetClosestPointTo(pt, vector3D, false, false);
            AddEntity(mline);
        }
        [CommandMethod("AddPolyline3d")]
        public static void AddPolyline3d()
        {
            Poly3dType poly3DType = Poly3dType.SimplePoly;
            Point3dCollection point3DCollection = new Point3dCollection
            {
                new Point3d(0, 0, 0),
                new Point3d(10, 0, 0),
                new Point3d(10, 10, 0),
                //new Point3d(20, 10, 0),
            };
            bool isClosed = false;
            Polyline3d polyline3D = new Polyline3d(poly3DType, point3DCollection, isClosed);
            //Poly3dType poly3DType1 = Poly3dType.CubicSplinePoly;
            //polyline3D.ConvertToPolyType(poly3DType1);
            //polyline3D.PolyType = poly3DType1;
            //polyline3D.Straighten();
            //polyline3D.SplineFit(Poly3dType.QuadSplinePoly, 100);
            //int n = 0;
            //foreach (PolylineVertex3d item in polyline3D)
            //{
            //    if (n == 1)
            //    {
            //        PolylineVertex3d polylineVertex3D = new PolylineVertex3d(new Point3d(20, 10, 0));
            //        polyline3D.InsertVertexAt(item, polylineVertex3D);
            //        break;
            //    }
            //    n++;
            //}
            AddEntity(polyline3D);
            Database db = HostApplicationServices.WorkingDatabase;
            using (Transaction trans = db.TransactionManager.StartTransaction())
            {
                polyline3D = polyline3D.ObjectId.GetObject(OpenMode.ForWrite) as Polyline3d;
                int n = 0;
                foreach (ObjectId item in polyline3D)
                {
                    if (n == 0)
                    {
                        PolylineVertex3d polylineVertex3D = new PolylineVertex3d(new Point3d(5, -5, 0));
                        polyline3D.InsertVertexAt(item, polylineVertex3D);
                        break;
                    }
                    n++;
                }
                trans.Commit();
            }
            //PolylineVertex3d polylineVertex3D = new PolylineVertex3d(new Point3d(10, 10, 0));
            //Database db = HostApplicationServices.WorkingDatabase;
            //using (Transaction trans = db.TransactionManager.StartTransaction())
            //{
            //    polyline3D = polyline3D.ObjectId.GetObject(OpenMode.ForWrite) as Polyline3d;
            //    polyline3D.AppendVertex(polylineVertex3D);
            //    trans.Commit();
            //}

        }
        [CommandMethod("AddPolyline2d")]
        public static void AddPolyline2d()
        {
            Point3dCollection pos = new Point3dCollection
            {
                new Point3d(0, 0, 0),
                new Point3d(10, 0, 0),
                new Point3d(10, 10, 0),
                new Point3d(20, 10, 0)
            };
            DoubleCollection doubles = new DoubleCollection
            {
                0,
                0,
                0,
                0
            };
            Polyline2d polyline2D = new Polyline2d(Poly2dType.SimplePoly, pos, 0, false, 0, 0, doubles);
            List<Point3d> point3Ds = new List<Point3d>();
            foreach (Vertex2d item in polyline2D)
            {
                Point3d point3D = polyline2D.VertexPosition(item);
                point3Ds.Add(point3D);
            }
            AddEntity(polyline2D);

        }
        [CommandMethod("AddDBText")]
        public static void AddDBText()
        {
            DBText dBText = new DBText();
            dBText.TextString = "AaGg";
            //dBText.Justify = AttachmentPoint.BaseRight;
            //dBText.HorizontalMode = TextHorizontalMode.TextCenter;
            //dBText.IsMirroredInX = true;
            dBText.WidthFactor = 0.8;
            dBText.Height = 450;
            //dBText.Rotation = -Math.PI / 6;
            dBText.Oblique = Math.PI / 6;
            bool bbb = dBText.IsDefaultAlignment;
            AddEntity(dBText);
            //string name = dBText.TextString;
            //using (Transaction trans = HostApplicationServices.WorkingDatabase.TransactionManager.StartTransaction())
            //{
            //    DBObject dBObject = dBText.TextStyleId.GetObject(OpenMode.ForRead);
            //}
        }
        [CommandMethod("AddRotatedDimension")]
        public static void AddRotatedDimension()
        {
            double rotation = 0;
            Point3d line1Point = new Point3d(0, 0, 0);
            Point3d line2Point = new Point3d(100, 0, 0);
            Point3d dimensionLinePoint = new Point3d(30, 40, 0);
            string dimensionText = "";
            Document doc = Application.DocumentManager.MdiActiveDocument;
            ObjectId dimensionStyle = doc.Database.DimStyleTableId;
            RotatedDimension rotatedDimension = new RotatedDimension(rotation, line1Point, line2Point, dimensionLinePoint, dimensionText, dimensionStyle);
            double o = rotatedDimension.Oblique;
            AddEntity(rotatedDimension);
        }
        [CommandMethod("AddAlignedDimension")]
        public static void AddAlignedDimension()
        {
            Point3d line1Point = new Point3d(0, 0, 0);
            Point3d line2Point = new Point3d(100, 0, 0);
            Point3d dimensionLinePoint = new Point3d(50, 30, 0);
            string dimensionText = "";
            Document doc = Application.DocumentManager.MdiActiveDocument;
            ObjectId dimensionStyle = doc.Database.DimStyleTableId;
            AlignedDimension alignedDimension = new AlignedDimension(line1Point, line2Point, dimensionLinePoint, dimensionText, dimensionStyle);
            alignedDimension.Oblique = Math.PI / 3;
            AddEntity(alignedDimension);
        }
        [CommandMethod("AddLineAngularDimension2")]
        public static void AddLineAngularDimension2()
        {
            Point3d line1Start = new Point3d(0, 0, 0);
            Point3d line1End = new Point3d(0, 50, 0);
            Point3d line2Start = new Point3d(10, 0, 0);
            Point3d line2End = new Point3d(50, 50, 0);
            Point3d arcPoint = new Point3d(30, 70, 0);
            string dimensionText = "";
            Document doc = Application.DocumentManager.MdiActiveDocument;
            ObjectId dimensionStyle = doc.Database.DimStyleTableId;
            LineAngularDimension2 lineAngularDimension2 = new LineAngularDimension2(line1Start, line1End, line2Start, line2End, arcPoint, dimensionText, dimensionStyle);
            AddEntity(lineAngularDimension2);
        }
        [CommandMethod("AddRadialDimension")]
        public static void AddRadialDimension()
        {
            Point3d center = new Point3d(0, 0, 0);
            Point3d chordPoint = new Point3d(100, 0, 0);
            double leaderLength = 50;
            string dimensionText = "";
            Document doc = Application.DocumentManager.MdiActiveDocument;
            ObjectId dimensionStyle = doc.Database.DimStyleTableId;
            RadialDimension radialDimension = new RadialDimension(center, chordPoint, leaderLength, dimensionText, dimensionStyle);
            AddEntity(radialDimension);
        }
        [CommandMethod("AddDiametricDimension")]
        public static void AddDiametricDimension()
        {
            Point3d chordPoint = new Point3d(50 / Math.Sqrt(2), 50 / Math.Pow(2, 0.5), 0);
            Point3d farChordPoint = new Point3d(-50 / Math.Pow(2, 0.5), -50 / Math.Pow(2, 0.5), 0);
            double leaderLength = 20;
            string dimensionText = "";
            Document doc = Application.DocumentManager.MdiActiveDocument;
            ObjectId dimensionStyle = doc.Database.DimStyleTableId;
            DiametricDimension diametricDimension = new DiametricDimension(chordPoint, farChordPoint, leaderLength, dimensionText, dimensionStyle);
            AddEntity(diametricDimension);
        }
        [CommandMethod("AddArcDimension")]
        public static void AddArcDimension()
        {
            Point3d centerPoint = new Point3d(0, 0, 0);
            Point3d xLine1Point = new Point3d(0, 50, 0);
            Point3d xLine2Point = new Point3d(50, 0, 0);
            Point3d arcPoint = new Point3d(70, 70, 0);
            string dimensionText = "";
            Document doc = Application.DocumentManager.MdiActiveDocument;
            ObjectId dimensionStyle = doc.Database.DimStyleTableId;
            ArcDimension arcDimension = new ArcDimension(centerPoint, xLine1Point, xLine2Point, arcPoint, dimensionText, dimensionStyle);
            arcDimension.ArcSymbolType = 1;
            arcDimension.HasLeader = true;
            AddEntity(arcDimension);
        }
        [CommandMethod("AddRadialDimensionLarge")]
        public static void AddRadialDimensionLarge()
        {
            Point3d center = new Point3d(0, 0, 0);
            Point3d chordPoint = new Point3d(50, 60, 0);
            Point3d overrideCenter = new Point3d(0, 25, 0);
            Point3d jogPoint = new Point3d(25, 37, 0);
            double jogAngle = Math.PI / 2;
            string dimensionText = "";
            Document doc = Application.DocumentManager.MdiActiveDocument;
            ObjectId dimensionStyle = doc.Database.DimStyleTableId;
            RadialDimensionLarge radialDimensionLarge = new RadialDimensionLarge(center, chordPoint, overrideCenter, jogPoint, jogAngle, dimensionText, dimensionStyle);
            AddEntity(radialDimensionLarge);
        }
        [CommandMethod("AddOrdinateDimension")]
        public static void AddOrdinateDimension()
        {
            bool useXAxis = true;
            Point3d definingPoint = new Point3d(50, 20, 0);
            Point3d leaderEndPoint = new Point3d(70, 50, 0);
            string dimensionText = "";
            Document doc = Application.DocumentManager.MdiActiveDocument;
            ObjectId dimensionStyle = doc.Database.DimStyleTableId;
            OrdinateDimension ordinateDimension = new OrdinateDimension(useXAxis, definingPoint, leaderEndPoint, dimensionText, dimensionStyle);
            ordinateDimension.Origin = new Point3d(50, 0, 0);
            AddEntity(ordinateDimension);
        }
        // 声明命令
        [CommandMethod("CreateBlock")]
        public static void CreateBlock()
        {
            // 获取当前文档和数据库
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;

            // 获取编辑器对象
            Editor ed = doc.Editor;

            // 提示用户选择实体
            PromptSelectionResult psr = ed.GetSelection();

            // 如果用户成功选择了实体
            if (psr.Status == PromptStatus.OK)
            {
                // 获取选择集中的对象ID集合
                ObjectIdCollection ids = new ObjectIdCollection(psr.Value.GetObjectIds());

                // 启动事务处理
                using (Transaction tr = db.TransactionManager.StartTransaction())
                {
                    // 打开块表并获取当前空间记录ID
                    BlockTable bt = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
                    ObjectId spaceId = db.CurrentSpaceId;

                    // 创建一个新的块表记录并命名为222
                    BlockTableRecord btr = new BlockTableRecord();
                    btr.Name = "222";

                    // 将新建的块表记录添加到块表中并升级打开权限以便修改
                    bt.UpgradeOpen();
                    bt.Add(btr);

                    // 通知事务处理器添加了新对象并降级打开权限以防止意外修改
                    tr.AddNewlyCreatedDBObject(btr, true);
                    bt.DowngradeOpen();

                    // 遍历选择集中的对象ID集合，将每个对象克隆到新建的块表记录中并删除原来的对象
                    foreach (ObjectId id in ids)
                    {
                        Entity ent = tr.GetObject(id, OpenMode.ForWrite) as Entity;
                        Entity cloneEnt = ent.Clone() as Entity;
                        btr.AppendEntity(cloneEnt);
                        tr.AddNewlyCreatedDBObject(cloneEnt, true);
                        ent.Erase();
                    }

                    // 提交事务处理并关闭事务处理器
                    tr.Commit();
                }

                ed.WriteMessage("\n已成功创建名为222的块。");
            }
        }
        // 声明命令
        [CommandMethod("InsertBlock")]
        public static void InsertBlock()
        {
            // 获取当前文档和数据库
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;

            // 获取编辑器对象
            Editor ed = doc.Editor;

            // 提示用户输入插入点
            PromptPointResult ppr = ed.GetPoint("\n请输入插入点：");

            // 如果用户成功输入了插入点
            if (ppr.Status == PromptStatus.OK)
            {
                // 获取插入点坐标
                Point3d pt = ppr.Value;

                // 启动事务处理
                using (Transaction tr = db.TransactionManager.StartTransaction())
                {
                    // 打开块表并获取名为222的块表记录ID
                    BlockTable bt = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
                    ObjectId btrId = bt["222"];

                    // 打开模型空间的块表记录并升级打开权限以便修改
                    BlockTableRecord ms = tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;

                    // 创建一个新的块引用对象，指定其块表记录ID和插入点
                    BlockReference br = new BlockReference(pt, btrId);

                    // 将新建的块引用对象添加到模型空间的块表记录中并通知事务处理器
                    ms.AppendEntity(br);
                    tr.AddNewlyCreatedDBObject(br, true);

                    // 打开名为222的块表记录并遍历其中的实体，如果有属性定义，则创建对应的属性引用对象，并将其添加到新建的块引用对象中
                    BlockTableRecord btr = tr.GetObject(btrId, OpenMode.ForRead) as BlockTableRecord;

                    foreach (ObjectId id in btr)
                    {
                        Entity ent = tr.GetObject(id, OpenMode.ForRead) as Entity;

                        if (ent is AttributeDefinition ad)
                        {
                            AttributeReference ar = new AttributeReference();
                            ar.SetAttributeFromBlock(ad, br.BlockTransform);
                            ar.TextString = ad.TextString;  // 这里可以根据需要修改属性值

                            br.AttributeCollection.AppendAttribute(ar);
                            tr.AddNewlyCreatedDBObject(ar, true);
                        }
                    }

                    // 提交事务处理并关闭事务处理器
                    tr.Commit();
                }

                ed.WriteMessage("\n已成功在模型空间中插入名为222的块。");
            }
        }
        [CommandMethod("AddLineSegment3d")]
        public static void AddLineSegment3d()
        {
            //扩展一下。求两个圆的四个切线
            Circle circle1 = new Circle(new Point3d(), Vector3d.ZAxis, 10);
            Circle circle2 = new Circle(new Point3d(50, 0, 0), Vector3d.ZAxis, 20);
            Curve3d curve1 = circle1.GetGeCurve();
            Curve3d curve2 = circle2.GetGeCurve();
            List<LineSegment3d> lines = new List<LineSegment3d>();
            for (double i = 0.1; i < Math.PI * 2; i += 0.1)
            {
                for (double j = 0.1; j < Math.PI * 2; j += 0.1)
                {
                    LineSegment3d line = new LineSegment3d();
                    line.Set(curve1, curve2, i, j);
                    if (lines.Any(l => l.StartPoint == line.StartPoint || l.EndPoint == line.EndPoint)) continue;
                    lines.Add(line);
                }
            }
            lines.ForEach(l => Curve.CreateFromGeCurve(l).AddEntity());
            circle1.AddEntity();
            circle2.AddEntity();
        }
        [CommandMethod("AddXline")]
        public static void AddXline()
        {
            Xline xline = new Xline();
            xline.BasePoint = new Point3d(10, 10, 0);
            xline.UnitDir = new Vector3d(20, 10, 0);
            xline.AddEntity();
        }
        [CommandMethod("AddRay")]
        public static void AddRay()
        {
            Point3d point = new Point3d();
            Vector3d vector = new Vector3d(-1, 1, 0);
            List<Curve> curves = SelectEntities<Curve>();
            if (curves.Count == 0) return;
            Curve curve = GetClosestCurve(point, vector, curves);
            if (curve == null) return;
            Curve curve1 = curve.Clone() as Curve;
            curve1.ColorIndex = 2;
            curve1.AddEntity();
        }
        private static Curve GetClosestCurve(Point3d point, Vector3d vector, List<Curve> curves)
        {
            Ray ray = new Ray();
            ray.BasePoint = point;
            ray.UnitDir = vector;
            Point3d closestPoint = new Point3d();
            double closestDis = double.MaxValue;
            Curve closestCur = null;
            foreach (var curve in curves)
            {
                Point3dCollection pos = new Point3dCollection();
                ray.IntersectWith(curve, Intersect.OnBothOperands, pos, IntPtr.Zero, IntPtr.Zero);
                foreach (Point3d po in pos)
                {
                    double dis = po.DistanceTo(point);
                    if (dis < closestDis)
                    {
                        closestPoint = po;
                        closestDis = dis;
                        closestCur = curve;
                    }
                }
            }
            return closestCur;

        }
        [CommandMethod("GetCurve")]
        public static void GetCurve()
        {
            LineSegment3d line3d = new LineSegment3d(new Point3d(), new Point3d(10, 10, 0));
            Line line = new Line(new Point3d(5, 5, 0), new Point3d(20, 0, 0));
            line.SetFromGeCurve(line3d);
        }
        [CommandMethod("GetCenterPl")]
        public static void GetCenterPl()
        {
            List<Polyline> pls = SelectEntities<Polyline>();
            if (pls.Count < 2) return;
            Polyline pl1 = pls[0];
            Polyline pl2 = pls[1];
            int n = 1000;
            double dis1 = pl1.Length / n;
            double dis2 = pl2.Length / n;
            Polyline pl = new Polyline();
            for (int i = 0; i < n; i++)
            {
                Point3d p1 = pl1.GetPointAtDist(dis1 * i);
                Point3d p2 = pl2.GetPointAtDist(dis2 * i);
                LineSegment3d line = new LineSegment3d(p1, p2);
                Point3d midpoint = line.MidPoint;
                if (pl.NumberOfVertices >= 2)
                {
                    Vector3d vector1 = pl.GetPoint3dAt(pl.NumberOfVertices - 1) - pl.GetPoint3dAt(pl.NumberOfVertices - 2);
                    Vector3d vector2 = midpoint - pl.GetPoint3dAt(pl.NumberOfVertices - 1);
                    double angle1 = vector1.GetAngleTo(Vector3d.XAxis, Vector3d.ZAxis);
                    double angle2 = vector2.GetAngleTo(Vector3d.XAxis, Vector3d.ZAxis);
                    if (Math.Abs(angle1 - angle2) < 0.001)
                    {
                        pl.SetPointAt(pl.NumberOfVertices - 1, midpoint.Convert2d(new Plane()));
                        continue;
                    }
                }
                pl.AddVertexAt(pl.NumberOfVertices, midpoint.Convert2d(new Plane()), 0, 0, 0);
            }
            pl.AddEntity();
        }
        [CommandMethod("Fangxiang")]
        public static void Fangxiang()
        {
            Entity ent = GetEntity();
            if (ent is Curve curve)
            {
                int n = 100;
                double dis = curve.GetDistanceAtParameter(curve.EndParam) / n;
                for (int i = 0; i < n; i++)
                {
                    Point3d point = curve.GetPointAtDist(dis * i);
                    Vector3d vector = curve.GetFirstDerivative(point);
                    vector = vector.GetNormal() * dis;
                    Vector3d v1 = vector.RotateBy(Math.PI / 6 * 5, Vector3d.ZAxis);
                    Vector3d v2 = vector.RotateBy(Math.PI / 6 * 5, -Vector3d.ZAxis);
                    Polyline pl = new Polyline();
                    pl.ColorIndex = 2;
                    pl.AddVertexAt(0, (point + v1).Convert2d(new Plane()), 0, 0, 0);
                    pl.AddVertexAt(1, point.Convert2d(new Plane()), 0, 0, 0);
                    pl.AddVertexAt(2, (point + v2).Convert2d(new Plane()), 0, 0, 0);
                    pl.AddEntity();
                    Application.DocumentManager.MdiActiveDocument.Editor.GetInteger("");
                    pl.EraseEntity();
                }
            }
        }
        public static Entity GetEntity()
        {
            Entity entity = null;
            Editor editor = Application.DocumentManager.MdiActiveDocument.Editor;
            PromptEntityResult per = editor.GetEntity("");
            if (per.Status == PromptStatus.OK)
            {
                Database db = HostApplicationServices.WorkingDatabase;
                using (Transaction trans = db.TransactionManager.StartTransaction())
                {
                    entity = (Entity)per.ObjectId.GetObject(OpenMode.ForRead);
                }
            }
            return entity;
        }
        public static List<T> SelectEntities<T>() where T : Entity
        {
            List<T> result = new List<T>();
            Editor editor = Application.DocumentManager.MdiActiveDocument.Editor;
            PromptSelectionResult psr = editor.GetSelection();
            if (psr.Status == PromptStatus.OK)
            {
                ObjectId[] objectids = psr.Value.GetObjectIds();
                Database database = HostApplicationServices.WorkingDatabase;
                using (Transaction tran = database.TransactionManager.StartTransaction())
                {
                    foreach (var item in objectids)
                    {
                        Entity entity = item.GetObject(OpenMode.ForRead) as Entity;
                        if (entity is T)
                        {
                            result.Add(entity as T);
                        }

                    }
                }
            }
            return result;
        }
        private static void AddEntity(this Entity entity)
        {
            //开启事务
            Database db = HostApplicationServices.WorkingDatabase;
            using (Transaction trans = db.TransactionManager.StartTransaction())
            {
                //打开块表
                BlockTable bt = (BlockTable)trans.GetObject(db.BlockTableId, OpenMode.ForRead);
                //打开块表记录
                BlockTableRecord btr = (BlockTableRecord)trans.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite);

                btr.AppendEntity(entity);
                trans.AddNewlyCreatedDBObject(entity, true);


                trans.Commit();
            }
        }
        private static void EraseEntity(this Entity entity)
        {
            Database db = HostApplicationServices.WorkingDatabase;
            using (Transaction trans = db.TransactionManager.StartTransaction())
            {
                entity.ObjectId.GetObject(OpenMode.ForWrite);
                entity.Erase();
                trans.Commit();
            }
        }
    }
}
